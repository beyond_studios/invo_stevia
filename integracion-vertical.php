<?php include('include/header.php'); ?>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area st-header-1" style="background-image:url('images/integracion-bg-1.jpg')">
	<div class="container">
		<div class="st-tbl">
			<div class="st-tbl-cell">
				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Integración Vertical</h1>
			</div>
		</div>
	</div>
</section>
<section class="st-middle-sec">
	<div class="st-common-sec st-integracion-vertical-sec">
		<div class="container">
			<div class="st-biofabricaBx">
				<h2 class="st-color-secondary">Integración Vertical</h2>
				<div class="row">
					<div class="col-md-6">
						<h3 class="st-bot-margin-30">La Integración Vertical es otra de las ventajas competitivas de Stevia One porque facilita:</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<ol class="st-ol-listing st-ol-listing-plain st-bullet-space" start="0">
							<li>El control de todos los procesos. Estos pueden planificarse de principio a fin.</li>
							<li>La eficiencia y sincronización de la cadena de abastecimiento.</li>
							<li>La mejora continua. Desarrollo continuo de nuevas variedades de alta calidad partiendo de nuestras plantas.</li>
						</ol>
					</div>
					
					<div class="col-md-6">
						<ol class="st-ol-listing st-ol-listing-plain st-bullet-space" start="3">							
							<li>El aseguramiento de la calidad de todo el proceso productivo, desde las semillas hasta el producto final. </li>
							<li>La garantía de que el proceso es sostenible socio-ambientalmente.</li>
							<li>Confianza y seguridad.</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="st-common-sec st-info-sec st-info-sec-1 st-integrecion-info-sec">
		<div class="container masqueBx">
			<h3 class="st-green st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.2s"><span class="disblk">En términos de eficiencia y sostenibilidad, la Integración Vertical permite una mejor optimización de los recursos.</span><br>Stevia One,<br>
Tenemos un compromiso con cada cliente.</h3>
		</div>
	</div>
	




<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->


	<div class="st-newsletter-hidden"><?php include('include/newsletter.php') ?></div>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</section>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<?php include('include/footer.php'); ?>