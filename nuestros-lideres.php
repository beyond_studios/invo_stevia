<?php include('include/header.php'); ?>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area" style="background-image:url('images/contact-bg.jpg')">
    <div class="container">
        <div class="st-tbl">
            <div class="st-tbl-cell">
                <h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Nuestros líderes<small></small></h1>
            </div>
        </div>
    </div>
</section>
<section class="st-middle-sec">
    <div class="st-common-sec st-theme-sec st-profile-sec">
        <div class="container">
            <div class="row st-profile-row">
                <div class="col-sm-4">
                    <span class="st-profile-pic-wrap st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0">
                        <img src="images/profile-1.jpg" alt="Marc Saverys" class="img-responsive" draggable="false">
                    </span>
                </div>
                <div class="col-sm-8">
                    <blockquote>
                        <p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">“Me siento entusiasmado por formar parte de este proyecto.  Nuestro compromiso con el planeta a través de Stevia One es ser parte de una solución para dos de los principales problemas del siglo 21: la obesidad y la diabetes. Asimismo, contribuiremos con el desarrollo de la economía peruana, motivando a nuestros accionistas a ser los líderes del mercado de Stevia. <br>
                        Haremos que nuestra gente se sienta orgullosa de ser parte de la familia de Stevia One”</p>
                        <div class="st-author-info st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">
                          <h5>Marc Saverys</h5>
                          <span class="st-author-post">President of the Board</span>
                        </div>
                    </blockquote>
                </div>
            </div>
        </div>
    </div>


    <div class="st-common-sec st-full-bg-columns">
        <div class="container-fluid">
            <div class="row st-tbl-row">
                <div class="col-sm-6">
                    <div class="st-full-bg-col-in">                        
                        <div class="row st-profile-row">
                            <div class="col-sm-4 st-small-profile-col">
                                <span class="st-profile-pic-wrap st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0">
                                    <img src="images/profile-6.jpg" alt="Filip Balcaen" class="img-responsive" draggable="false">
                                </span>
                                <div class="st-author-info st-author-info-bottom st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">
                                  <h5>Sebastiaan Saverys</h5>
                                  <span class="st-author-post">Executive Director</span>                                   
                                </div>
                            </div>
                            <div class="col-sm-8 st-small-profile-desc-col">
                                <blockquote>
                                    <p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">“Siempre me pregunté, qué es lo que podía hacer para mejorar el mundo. Hoy, sé que lo podemos hacer de una manera muy eficiente con stevia. Estoy convencido de que como buenos productores y comercializadores de stevia podremos ayudar a solucionar un problema de salud global. Y si cada día, podemos inspirar a alguien, ¡Podemos inspirar el planeta!”</p>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 st-bg-gray">
                    <div class="st-full-bg-col-in">                        
                        <div class="row st-profile-row">
                            <div class="col-sm-4 st-small-profile-col">
                                <span class="st-profile-pic-wrap st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0">
                                    <img src="images/profile-2.jpg" alt="Sebastiaan Saverys" class="img-responsive" draggable="false">
                                </span>
                                <div class="st-author-info st-author-info-bottom st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">
                                  <h5>Filip Balcaen</h5>
                                  <span class="st-author-post">Board Member</span>
                                </div>
                            </div>
                            <div class="col-sm-8 st-small-profile-desc-col">
                                <blockquote>
                                    <p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">“Me entusiasma apoyar a Stevia One a ser el referente en la industria de Stevia por su producción trazable basada en tecnología sustentable y calidad. Creo firmemente que el estilo de vida es importante para luchar contra la obesidad y la diabetes, y a través de Stevia One, seremos capaces de proporcionar la solución a una dieta más balanceada.”</p>
                                </blockquote>                                    
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row st-tbl-row st-tbl-row-invert">                
                <div class="col-sm-6">
                    <div class="st-full-bg-col-in">                        
                        <div class="row st-profile-row">
                            <div class="col-sm-4 st-small-profile-col">
                                <span class="st-profile-pic-wrap st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0">
                                    <img src="images/profile-3.jpg" alt="Augusto Baertl" class="img-responsive" draggable="false">
                                </span>
                                <div class="st-author-info st-author-info-bottom  st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">
                                  <h5>Augusto Baertl</h5>
                                  <span class="st-author-post">Board Member</span>                                   
                                </div>
                            </div>
                            <div class="col-sm-8 st-small-profile-desc-col">
                                <blockquote>
                                    <p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">“Me siento emocionado de contribuir con un equipo que demuestra lo que realmente es construir los cimientos de la empresa del futuro. Me siento comprometido con este proyecto que beneficia a las regiones en las cuales operamos en el Perú.”</p>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 st-bg-theme">
                    <div class="st-full-bg-col-in">                        
                        <div class="row st-profile-row">
                            <div class="col-sm-4 st-small-profile-col">
                                <span class="st-profile-pic-wrap st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0">
                                    <img src="images/profile-7.jpg" alt="Gilles Plaquet" class="img-responsive" draggable="false">
                                </span>
                                <div class="st-author-info st-author-info-bottom st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">
                                  <h5>Gilles Plaquet</h5>
                                  <span class="st-author-post">Board Member</span>                                   
                                </div>
                            </div>
                            <div class="col-sm-8 st-small-profile-desc-col">
                                <blockquote>
                                    <p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">“Me siento parte de un equipo sólido, el éxito está en marcha.  Estoy seguro que Stevia One contribuirá a luchar contra la obesidad y la diabetes en el mundo; este es el desafío de los “milenials”. Stevia One ofrece una solución natural y efectiva para continuar disfrutando la dulzura de la vida.”</p>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row st-tbl-row">
                <div class="col-sm-6">
                    <div class="st-full-bg-col-in">                        
                        <div class="row st-profile-row">
                            <div class="col-sm-4 st-small-profile-col">
                                <span class="st-profile-pic-wrap st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0">
                                    <img src="images/profile-4.jpg" alt="Leslie Pierce Diez Canseco" class="img-responsive" draggable="false">
                                </span>
                                <div class="st-author-info st-author-info-bottom st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">
                                  <h5>Leslie Pierce Diez Canseco</h5>
                                  <span class="st-author-post">Board Member</span>                                  
                                </div>
                            </div>
                            <div class="col-sm-8 st-small-profile-desc-col">
                                <blockquote>
                                    <p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">“Me siento comprometido y orgulloso de ser parte de un proyecto como Stevia One, cuyo objetivo primordial es contribuir a la salud de las personas y el bienestar de su entorno. Estoy convencido de que, en un futuro próximo,  esta compañía marcará las pautas de lo que significa ser socialmente responsable cuidando la salud del planeta. Hoy, me siento parte de un sueño que le hará bien a nuestro mundo”</p>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 st-bg-gray">
                    <div class="st-full-bg-col-in">                        
                        <div class="row st-profile-row">
                            <div class="col-sm-4 st-small-profile-col">
                                <span class="st-profile-pic-wrap st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0">
                                    <img src="images/profile-5.jpg" alt="Felipe Ramos Guevara" class="img-responsive" draggable="false">
                                </span>
                                <div class="st-author-info st-author-info-bottom st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">
                                  <h5>Felipe Ramos Guevara</h5>                                  
                                  <span class="st-author-post">CEO</span>
                                </div>
                            </div>
                            <div class="col-sm-8 st-small-profile-desc-col">
                                <blockquote>
                                    <p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">“Me enorgullece regresar al Perú y ser parte de un equipo maravilloso, en una compañía con un propósito tan elevado como Stevia One, que se preocupa por la gente, por el planeta e inspira a otras compañías a hacer lo mismo.”</p>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>
    

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                    NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->


    <?php include('include/newsletter.php') ?>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</section>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<?php include('include/footer.php'); ?>