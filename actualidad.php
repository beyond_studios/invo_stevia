<?php include('include/header.php'); ?>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area st-header-1" style="background-image:url('images/stevia-web-hechos-bg-1.jpg')">
    <div class="container">
        <div class="st-tbl">
            <div class="st-tbl-cell">
                <h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Actualidad</h1>
            </div>
        </div>
    </div>
</section>
<section class="st-middle-sec">

    <div class="st-common-sec st-twocol-desc-sec st-actualidad-sec">
        <div class="container">
            <div class="row">
                <div class="col-sm-6" id="mision">
                    <div class="st-full-bg-col-in">
                        <h2 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">¡Obesidad! Una enfermedad de peso</h2>                        
                        <ul class="st-bullet-list st-bullet-list-plain st-bullet-space st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.5s">
                            <li><strong>Hechos</strong></li>
                            <li>Millones de personas alrededor del mundo son obesas.</li>
                            <li>2012, la OMS estimó que más de 40 millones de niños menores de 5 años ya tenían sobrepeso o eran obesos.</li>
                            <li><strong>2030, la obesidad será una de las principales causas de muerte.</strong></li>
                            <li>Vida sedentaria, malos hábitos alimenticios o la mezcla de ambos son las principales causas de la obesidad. </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6" id="vision">
                    <div class="st-full-bg-col-in">
                        <h2 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Las caries, enemigas de la salud bucal</h2>                        
                        <ul class="st-bullet-list st-bullet-list-plain st-bullet-space st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.5s">
                            <li><strong>Hechos</strong></li>
                            <li><strong>Alrededor del mundo, 90% de niños en edad escolar y casi el 100% de adultos tienes caries.</strong> </li>
                            <li>30% de adultos entre los 65 – 74 años no tienen dientes naturales.</li>
                            <li><span class="st-color-primary">Las caries son el enemigo número uno de la salud dental. <br><strong>¡Hoy, el azúcar tiene un sabor amargo!</strong></span></li>
                        </ul>
                        
                    </div>
                </div>
            </div>  
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="st-twocol-extra-col">
                        <h2 class="st-color-secondary st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">La diabetes, el dulce mal</h2>
                        <p class="st-font-light st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">La diabetes es una enfermedad crónica que eleva los niveles de glucosa de la sangre, pudiendo dañar ojos y riñones. <br><br>
                        ¿Y dónde está la glucosa? En los alimentos. </p>
                        <p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.3s"><strong>Hechos</strong></p>
                        <ul class="st-bullet-list st-bullet-list-plain st-bullet-space st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.7s">                            
                            <li class="st-font-light">2013, la OMS estimó que 347 millones de personas en el mundo ya tenían 
                            diabetes.  </li>
                            <li><strong>Hacia el 2035, la International Diabetes Federation (IDF), estima que 592 millones de personas padecerán de diabetes. Esto le costará al sistema de salud unos US$ 500 billones, más del 10% del presupuesto de salud a nivel mundial.</strong></li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


    

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                    NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->


    <div class="st-newsletter-hidden"><?php include('include/newsletter.php') ?></div>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</section>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<?php include('include/footer.php'); ?>