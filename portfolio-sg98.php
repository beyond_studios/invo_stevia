<?php include('include/header.php'); ?>
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area st-header-1" style="background-image: url('images/banner_kurmi63-1.jpg')">
	<div class="container">
		<div class="st-tbl">
			<div class="st-tbl-cell">
				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">KURMI-63</h1>
			</div>
		</div>
	</div>
</section>
<section class="st-middle-sec">
	<div class="st-common-sec st-twocol-desc-sec st-portfolio-sg98-sec">
		<div class="container">
			<div class="row">
				<div class="col-sm-6" id="mision">
					
						<h2 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">DESCRIPCIÓN</h2>
						<p class="st-animate st-top-margin-25" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">SG98 Reb A 63 de esteviol glucósidos es un edulcorante natural no calórico y extraído de las hojas de la planta de stevia. No contiene calorías, posee un alto dulzor entre 150 - 200 veces más dulce que el azúcar.</p>
					
				</div>
				<div class="col-sm-6" id="vision">					
						<h2 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">CARACTERÍSTICAS</h2>
						<ul class="st-bullet-list st-bullet-list-plain st-bullet-space  st-animate st-top-margin-25" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
							<Li>Alta solubilidad en agua y bases alcohólicas. </Li>
							<Li>Alta estabilidad en soluciones ácidas y alcalinas (pH4-9). </Li>
							<Li>Alta tolerancia a temperaturas de hasta 196ºC. </Li>
							<Li>Alta estabilidad bajo luz. </Li>
							<Li>No daña los dientes. </Li>
							<li>Todo natural, sin ingredientes de síntesis química. </Li>
							<li>No fermentado. </Li>
						</ul>					
				</div>
			</div>
		</div>
	</div>
	<div class="st-common-sec st-image-sec st-image-sec-1" style="background-image: url('images/applicationes-bg.jpg')">
		<div class="container container-xl">
			<h2 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Aplicaciones</h2>
			<div class="row">
				<div class="col-sm-6">
					<ul class="st-bullet-list st-bullet-list-plain st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
						<li>Como un edulcorante natural para productos de alimentos y bebidas.</li>
					</ul>
				</div>
				<!--<div class="col-sm-6">
					<ul class="st-icon-list st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.5s">
						<li><span>Solicitar Muestra</span><i class="sicon-muestra"></i></li>
						<li><span>Descargar Ficha Técnica</span><i class="sicon-d-arrow-down"></i></li>
					</ul>
				</div>-->
			</div>
		</div>
	</div>
	<!--<div class="st-common-sec st-product-detail-big-btns-sec">
		<div class="container">
			<div class="row st-big-btn-row">
				<div class="col-xs-6 text-right">
					<a href="#" class="xtra-lg-btn st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s"><i class="sicon-arrow-s-left btnArrow"></i><span>ver nuestros<br>anterior
					</span></a>
				</div>
				<div class="col-xs-6">
					<a href="#" class="xtra-lg-btn st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.5s"><span>ver producto<br> siguiente
					</span><i class="sicon-arrow-s-right btnArrow"></i></a>
				</div>
			</div>
		</div>
	</div>-->
	
	<div class="st-common-sec st-product-detail-form st-contact-form-sec st-contact-form-sec-new st-portfolio-sg98-contact">
		<div class="container">
			<h3 class="st-green st-animate text-uppercase" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">Contáctese con nosotros</h3>
			<?php include('include/contact.php')?>
		</div>
	</div>
	<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                    NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
    <div class="st-newsletter-hidden"><?php include('include/newsletter.php')?></div>
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
</section>
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<?php include('include/footer.php'); ?>