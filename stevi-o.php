<?php
include('include/header.php');
?>
<section class="st-header-area st-header-1" style="background-image: url('images/background-productos.png')">
	<div class="container">
		<div class="st-tbl">
			<div class="st-tbl-cell">
				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Productos</h1>
			</div>
		</div>
	</div>
</section>
<section class="st-middle-sec">
	<section class="container title">
		<div class="row">
			<div class="col-xs-12">
				<h2>STEVI-O</h2>
			</div>
		</div>
	</section>
	<section class="container body-content">
		<div class="row">
			<div class="col-xs-12">
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<h3>Extracto de stevia</h3>
						<p>Stevi-o es un edulcorante natural, no calórico extraído de las hojas de stevia de la planta. No contiene altas calorías sino un alto dulzor, que es entre 150 y 200 veces más dulce que el azúcar de caña.</p>
						<h3>Características:</h3>
						<ul>
							<li>Alta solubilidad en bases de agua y alcohol</li>
							<li>Alta estabilidad en soluciones ácidas y alcalinas (pH 4-9)</li>
							<li>Alta tolerancia a temperaturas de hasta 196ºC</li>
							<li>Alta estabilidad bajo luz</li>
							<li>No afecta la dentadura</li>
							<li>Todo natural, sin ingredientes de síntesis química</li>
							<li>No fermenta</li>
						</ul>
						<h3>Aplicaciones:</h3>
						<p>Como edulcorante natural para productos alimenticios y bebidas.</p>
					</div>
					<div class="col-xs-12 col-sm-6">
						<img src="images/stevi-o/stevio-big.png" title="Stevi-O" class="hidden-xs" />
						<p class="buttons transparent"><a href="#" class="button-transparent">DESCARGA FICHA TÉCNICA</a></p>
						<i class="leaf hidden-xs hidden-sm hidden-md"></i>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p class="buttons"><a href="contactenos.php" class="button">CONTÁCTENOS</a></p>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>
<?php
include('include/footer.php');