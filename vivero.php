<?php include('include/header.php'); ?>



<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

												MIDDLE SECTION

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<section class="st-header-area st-header-1" style="background-image:url('images/nuestra-bg-1.jpg')">

	<div class="container">

		<div class="st-tbl">

			<div class="st-tbl-cell">

				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">vivero</h1>

			</div>

		</div>

	</div>

</section>

<section class="st-middle-sec">

	<div class="st-common-sec st-vivero-sec">

		<div class="container">			
			<div class="st-vivero-desc">
				<h2 class="st-bot-margin-40">vivero</h2>

				<ul class="st-bullet-list st-bullet-list-plain st-bullet-space">

					<li>Es el lugar de aclimatación de las plantas que provienen de la biofábrica.</li>

					<li>Tiene la función de propagar las plantas que provienen de esquejes.</li>

					<li>Es el lugar donde se lleva a cabo la producción de plantines.</li>

				</ul>
			</div>

		</div>

	</div>

	

	<div class="st-common-sec st-info-sec st-testimonials-sec st-info-vivero-sec">

		<div class="container">

			

                

                <blockquote>

                    <p class="st-animate st-bot-margin-30" data-os-animation="fadeInUp" data-os-animation-delay="0s">“Stevia One es mi segunda familia. Ya estoy casi 6 años. Es mi primer empleo, y más que un trabajo venir aquí me hace sentir completo.</p>

                    <p class="st-animate"  data-os-animation="fadeInUp" data-os-animation-delay="0s">Nuestras políticas de seguridad, cuidado de medio ambiente, manejo de residuos y uso eficiente del agua se han convertido en valores que comparto con  mi familia. Y estoy seguro que no soy el único, esto se extiende a toda la familia Stevia One”.</p>

                    <div class="st-author-info st-author-info-1 st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s" style="animation-delay: 0.2s;">

                        <h5>David Paredes</h5>

                        <span class="st-author-post">Jefe de Vivero</span>

                        <span class="st-author-company">Ingeniero Agrónomo</span>

                    </div>

                </blockquote>

                

		</div>

	</div>





<div class="st-newsletter-hidden"><?php include('include/newsletter.php') ?></div>

</section>





<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

												MIDDLE SECTION END

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->



<?php include('include/footer.php'); ?>