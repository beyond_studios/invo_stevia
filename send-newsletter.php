<?php
error_reporting(E_ALL);
include ('PHPMailer-master/PHPMailerAutoload.php');

$response=new stdClass();
$response->isError=FALSE;
$response->message='El mensaje se ha enviado con éxito';
try
{
	$nombre=isset($_POST['textBoxNombreCompleto'])?trim($_POST['textBoxNombreCompleto']):"";
	$email=isset($_POST['textBoxEmail'])?trim($_POST['textBoxEmail']):"";
	if (trim($email)=='')
		throw new Exception("El campo e-mail es obligatorio.");
	$html="";
	$html.="Se ha recibido una solicitud de suscripción a newsletter. A continuación los datos:<br />";
	$html.="<br />";
	$html.="Nombre completo: ".$nombre."<br />";
	$html.="E-Mail: ".$email."<br />";
	$mail=new PHPMailer(true);
	try
	{
		$mail->From=$email;
		$mail->FromName=$nombre;
		$mail->setFrom($email, $nombre);
		//$mail->addAddress('luis@involucra.com', 'Luis Guillén');
		$toEmail='tgrana@steviaone.com';
		$mail->addAddress($toEmail, 'Stevia One');
		$mail->addAddress('eduardo@involucra.com', 'Eduardo Páez Trujillo');
		$mail->Subject='Suscripción al newsletter del sitio Stevia One';
		$mail->IsHTML(true);
		$mail->CharSet='UTF-8';
		$mail->Body=$html;
		$mail->send();
	}
	catch ( phpmailerException $e )
	{
		throw new Exception($e->errorMessage());
	}
	catch ( Exception $e )
	{
		throw new Exception($e->getMessage());
	}
}
catch ( Exception $ex )
{
	$response->isError=TRUE;
	$response->message=$ex->getMessage();
}
echo json_encode($response);