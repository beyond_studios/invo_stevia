<?php
$pageName=basename($_SERVER['PHP_SELF']);
$pageName_class='st-page-'.str_replace(array('.php',' '), '', $pageName);
if($pageName=='index.php'){
	$pageName_class.=' loading';
}
if(session_id()=='')
{
	$lc="";
	if(isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
		$lc=substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
	if($lc!='es')
	{
		// header("Location: http://www.steviaone.com/en/");
		// exit();
	}
}
session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>SteviaOne - Passionate about health</title>
<link rel="apple-touch-icon" sizes="57x57" href="images/icons/apple-icon-57x57.png" />
<link rel="apple-touch-icon" sizes="60x60" href="images/icons/apple-icon-60x60.png" />
<link rel="apple-touch-icon" sizes="72x72" href="images/icons/apple-icon-72x72.png" />
<link rel="apple-touch-icon" sizes="76x76" href="images/icons/apple-icon-76x76.png" />
<link rel="apple-touch-icon" sizes="114x114" href="images/icons/apple-icon-114x114.png" />
<link rel="apple-touch-icon" sizes="120x120" href="images/icons/apple-icon-120x120.png" />
<link rel="apple-touch-icon" sizes="144x144" href="images/icons/apple-icon-144x144.png" />
<link rel="apple-touch-icon" sizes="152x152" href="images/icons/apple-icon-152x152.png" />
<link rel="apple-touch-icon" sizes="180x180" href="images/icons/apple-icon-180x180.png" />
<link rel="icon" type="image/png" sizes="192x192" href="images/icons/android-icon-192x192.png" />
<link rel="icon" type="image/png" sizes="32x32" href="images/icons/favicon-32x32.png" />
<link rel="icon" type="image/png" sizes="96x96" href="images/icons/favicon-96x96.png" />
<link rel="icon" type="image/png" sizes="16x16" href="images/icons/favicon-16x16.png" />
<link rel="manifest" href="images/icons/manifest.json" />
<link rel="mask-icon" href="images/icons/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#ffffff" />
<meta name="msapplication-TileImage" content="images/icons/ms-icon-144x144.png" />
<meta name="theme-color" content="#ffffff" />
<link href="css/style.min.css?ver=1.0.0" rel="stylesheet" />
</head>

<body class="<?php if($pageName !== 'index.php' & $pageName !== 'index-demo.php'){echo 'st-innerpage ';} echo $pageName_class; echo ($pageName == 'stevia-tomorrow-today.php'? ' st-navbar-light':''); ?>">
	<?php
	if($pageName == 'index.php' || $pageName == 'index-demo.php'){?>
	<div class='preloader'>
		<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" style="display: none;">
		  <symbol id="wave">
		    <path d="M420,20c21.5-0.4,38.8-2.5,51.1-4.5c13.4-2.2,26.5-5.2,27.3-5.4C514,6.5,518,4.7,528.5,2.7c7.1-1.3,17.9-2.8,31.5-2.7c0,0,0,0,0,0v20H420z"></path>
		    <path d="M420,20c-21.5-0.4-38.8-2.5-51.1-4.5c-13.4-2.2-26.5-5.2-27.3-5.4C326,6.5,322,4.7,311.5,2.7C304.3,1.4,293.6-0.1,280,0c0,0,0,0,0,0v20H420z"></path>
		    <path d="M140,20c21.5-0.4,38.8-2.5,51.1-4.5c13.4-2.2,26.5-5.2,27.3-5.4C234,6.5,238,4.7,248.5,2.7c7.1-1.3,17.9-2.8,31.5-2.7c0,0,0,0,0,0v20H140z"></path>
		    <path d="M140,20c-21.5-0.4-38.8-2.5-51.1-4.5c-13.4-2.2-26.5-5.2-27.3-5.4C46,6.5,42,4.7,31.5,2.7C24.3,1.4,13.6-0.1,0,0c0,0,0,0,0,0l0,20H140z"></path>
		  </symbol>
		</svg>
		<div class="box">
		  <div class="percent">
		    <div class="percentNum" id="count">0</div>
		    <div class="percentB">%</div>
		  </div>
		  <div id="water" class="water">
		    <svg viewBox="0 0 560 20" class="water_wave water_wave_back">
		      <use xlink:href="#wave"></use>
		    </svg>
		    <svg viewBox="0 0 560 20" class="water_wave water_wave_front">
		      <use xlink:href="#wave"></use>
		    </svg>
		  </div>
		</div>
	</div>
<?php } ?>
	<div class="st-body-wrap">
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container">
				<button type="button" class="navbar-toggle collapsed pull-right visible-xs visible-sm" aria-expanded="false">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
				</button>
				<div class="navbar-header">
					<a class="navbar-brand" href="./"> SteviaOne </a>
				</div>
			</div>
			<div class="st-dropmenu">
				<div class="container">
					<button type="button" class="navbar-toggle collapsed pull-right hidden-sm hidden-xs" aria-expanded="false">
						<span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
					</button>
					<div class="st-main-menu-wrap">
						<div class="container">
							<div class="text-right st-lng-menu">
								<ul class="breadcrumb st-mar-t-30">
									<li><a href="/<?php echo $pageName; ?>">ES</a></li>
									<li><a href="/en/<?php echo $pageName; ?>">EN</a></li>
								</ul>
							</div>
							<div class="row st-main-menu">
								<div class="col-md-2 col-sm-4 hidden-xs hidden-sm visible-md-up">
									<a class="navbar-brand" href="./"> SteviaOne </a>
								</div>
								<div class="col-md-2 col-sm-4 st-mar-t-30">
									<h4>
										<a href="quienes-somos.php">QUIÉNES SOMOS</a>
									</h4>
								</div>
								<div class="col-md-2 col-sm-4 st-mar-t-30">
									<h4>
										<a href="nuestra-propuesta-unica.php">PROPUESTA ÚNICA</a>
									</h4>
								</div>
								<div class="col-md-2 col-sm-4 st-mar-t-30">
									<h4>
										<a href="productos.php">PRODUCTOS</a>
									</h4>
								</div>
								<div class="col-md-2 col-sm-4 st-mar-t-30">
									<h4>
										<a href="#">CONTÁCTENOS</a>
									</h4>
									<ul class="nav nav-pills nav-stacked  collapse in" aria-expanded="true">
										<li><a href="contactenos.php">Nuestras Oficinas</a></li>
										<li><a href="trabaja-con-nosotros.php">Trabaja con Nosotros</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</nav>