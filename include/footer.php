<footer class="st-footer">
	<div class="st-footer-main">
		<div class="container">
			<div class="row">
				<div class="col-sm-2 col-xs-6">
					<a href="./" class="navbar-brand active"> SteviaOne </a>
				</div>
				<div class="col-sm-4 col-xs-6">
					<div class="media row-first">
						<div class="media-left">
						</div>
						<div class="media-body">
							<h5>OFICINA CENTRAL</h5>
						</div>
					</div>
					<div class="media">
						<div class="media-left">
							<i class="sicon-favourite"></i>
						</div>
						<div class="media-body">
							<h5>PERÚ</h5>
							<p>Av. Dionisio Derteano 184 Of. 704, <br>San Isidro, Lima, Perú</p>
						</div>
					</div>
				</div>
				<div class="col-sm-3 col-xs-6">
					<div class="media row-first">
					</div>
					<div class="media">
						<div class="media-left">
							<i class="sicon-phone-1"></i>
						</div>
						<div class="media-body">
							<h5>TELÉFONO</h5>
							<p>
								<a href="tel:5112535578">+51-1-253-5578</a><br> <a href="tel:5112535580">+51-1-253-5580</a>
							</p>
						</div>
					</div>
				</div>
				<div class="col-sm-3 col-xs-6">
					<div class="media row-first">
					</div>
					<div class="media">
						<div class="media-left">
							<i class="sicon-email-1"></i>
						</div>
						<div class="media-body">
							<h5>EMAIL</h5>
							<p>
								<a href="mailto:sales@steviaone.com">stevia@steviaone.com</a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="st-copyright">
		<p>Copyright © 2018 Stevia One S.A.</p>
	</div>
</footer>
</div>
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/parallax.js"></script>

<script src="js/mobile-detect.min.js"></script>
<script src="js/jquery.waypoints.min.js"></script>
<script src="js/bootstrap-select.min.js"></script>
<script src="js/validate/jquery.validate.min.js"></script>
<script src="js/jquery.form.min.js"></script>
<script src="js/newsletter.js"></script>
<script src="js/video/video.min.js"></script>
<script src="js/video/videojs-background.min.js"></script>
<script src="js/jquery.scrollify.js"></script>
<script src="sjs/custom.js"></script>
<script src="js/contact.js"></script>
<script src="js/workwithus.js"></script>
<script type="text/javascript">
/* BEGIN AG Google Analytics Plugin v.1.0.8 */
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www."); document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
 </script>
<script type="text/javascript">
 try { var pageTracker = _gat._getTracker("UA-32194108-1");  pageTracker._setCustomVar( 1, "user_ip", "198.49.67.162", 1 );    var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-32194108-1']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
  pageTracker._trackPageview();
 } catch(err) {}
/* END AG Google Analytics Plugin v.1.0.8 */
/* ========== www.gordejev.lv =========== */
</script>
<?php
if($pageName == 'contact.php')
{
?>
<script>
      function initMap() {
        var mapDiv = document.getElementById('st-map');
        var map = new google.maps.Map(mapDiv, {
            center: {lat: -12.096614, lng: -77.028490},
            zoom: 18,
            scrollwheel: false,
        });
       // Create a marker and set its position.
        var marker = new google.maps.Marker({
       map: map,
       position: {lat: -12.096614, lng: -77.028490},
       title: 'Av. Juan de Arona 720 Oficina 302, San Isidro, Lima, Perú.'
        });
          }
    </script>
<script src="https://maps.googleapis.com/maps/api/js?callback=initMap" async defer></script>
<?php
}
?>
</body>
</html>