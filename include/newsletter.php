
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<div class="st-common-sec st-newsletter-sec">
	<div class="container">
		<div class="row st-newsletter-row">
			<div class="col-md-7">
				<div class="media st-info-media">
					<div class="media-left">
						<i class="sicon-news"></i>
					</div>
					<div class="media-body">
						<h4>STEVIA NEWS</h4>
						<p>Noticias, nuevos desarrollos y aplicaciones. Suscríbase a nuestro newsletter.</p>
					</div>
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-horizontal">
					<div class="st-form-success">
						<h4>Su mensaje ha sido enviado con éxito.</h4>
					</div>
					<div class="st-form-error">
						<h4>Ocurrió un error al enviar su mensaje.</h4>
					</div>
					<form id="formNewsletter">
						<div class="form-group">
							<div class="col-sm-7">
								<input type="text" class="form-control" placeholder="Nombre" name="textBoxNombreCompleto"> <span class="helper-block"></span>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-7">
								<input type="email" class="form-control" placeholder="Email" name="textBoxEmail"> <span class="helper-block"></span>
							</div>
							<div class="col-sm-5">
								<button class="btn btn-default btn-block" type="submit">ENVIAR</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
