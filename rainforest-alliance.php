<?php include('include/header.php'); ?>



<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

												MIDDLE SECTION

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<section class="st-header-area" style="background-image:url('images/rainforest-bg.jpg')">

	<div class="container">

		<div class="st-tbl">

			<div class="st-tbl-cell">

				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Rainforest Alliance <br>Certified™<small></small></h1>

			</div>

		</div>

	</div>

</section>

<section class="st-middle-sec">

	<div class="st-common-sec st-theme-sec">

		<div class="container container-lg">

			<div class="st-biofabricaBx">

				<h2 class="st-color-secondary"><span>Rainforest<br>Alliance<br>Certified™</span> 

					<span class="st-heading-img">

						<img src="images/rainforest.svg" alt="Rainforest Alliance Certified">

					</span>

				</h2>

				<div class="row trazablesBx">

					<div class="col-md-6 col-sm-5">

						<p>Después de mucho esfuerzo y trabajo continuo, en Stevia One nos enorgullece contar con el sello Rainforest Alliance Certified™. Somos capaces de transformar las prácticas de uso de suelo, las prácticas empresariales y el comportamiento de los consumidores, entre otros.</p>

					</div>

					<div class="col-md-1 hiddne-sm">

					</div>

					<div class="col-md-5 col-sm-7">

						<p class="st-bot-margin-30">El sello Rainforest Alliance Certified™ es un símbolo reconocido internacionalmente de sostenibilidad ambiental, social y económica. Estos ejes se rigen a su vez bajo la norma para Agricultura Sostenible dictada por la Red de Agricultura Sostenible (RAS), otra organización que trabaja en conjunto con Rainforest Alliance. 

</p>

					</div>

				</div>

			</div>

		</div>

	</div>

	

    <div class="st-common-sec st-info-sec">

		<div class="container container-lg rainBx">

			<h5>La norma está basada en <span>10 principios:</span></h5>

			<div class="row">

				<div class="col-sm-6">

					<ol class="st-ol-listing st-bullet-space" start="0">

						<li>Sistema de gestión social y ambiental<br>

							<p>Son las políticas y procedimientos que se utilizan para planificar y ejecutar las operaciones que fomenten la implementación de las buenas prácticas de manejo.</p>

						</li>

						<li>Conservación de ecosistemas<br>

							<p>Los campos certificados protegen los ecosistemas naturales y realizan actividades para recuperar ecosistemas degradados.</p>

						</li>

						<li>Protección de la vida silvestre<br>

							<p>Los campos certificados son refugios para la vida silvestre residente y migratoria, especialmente para las especies amenazadas o en peligro de extinción.</p>

						</li>

						<li>Conservación de recursos hídricos<br>

							<p>El agua es vital. Los campos certificados realizan acciones para conservar el agua, evitar su desperdicio y prevenir su contaminación.</p>

						</li>

						<li>Trato justo y buenas condiciones para los trabajadores<br>

							<p>Los trabajadores que laboran en campos certificados y las familias que viven en ellos gozan de derechos y condiciones expresados por la ONU en la Declaración Universal de los Derechos Humanos, en la Convención sobre los Derechos de Niños y los convenios de la OIT.</p>

						</li>

					</ol>

				</div>

				<div class="col-sm-6 st-padding-top-0">

					<ol class="st-ol-listing st-bullet-space" start="5">

						<li>Salud y seguridad ocupacional<br>

							<p>Los campos certificados cuentan con un programa de salud y seguridad ocupacional para reducir o prevenir los riesgos de accidentes en el lugar de trabajo.</p>

						</li>

						<li>Relaciones con la comunidad<br>

							<p>Los campos certificados se relacionan positivamente con las comunidades cercanas y los grupos de interés locales. Contribuyen al desarrollo económico local mediante la capacitación y el empleo.</p>

						</li>

						<li>Manejo integrado del cultivo.<br>

							<p>Los campos certificados contribuyen a la eliminación del uso de productos químicos reconocidos por su impacto negativo en la salud humana y los recursos naturales. El manejo integrado de cultivos disminuye los riesgos y efectos de infestaciones de plagas.</p>

						</li>

						<li>Manejo y conservación de suelos.<br>

							<p>Los campos certificados realizan actividades para prevenir o controlar la erosión y así disminuir la pérdida de nutrientes y los impactos negativos en los cuerpos de agua.</p>

						</li>

						<li>Manejo integrado de desechos.<br>

							<p>Los campos certificados están ordenados y limpios, sus trabajadores y habitantes cooperan con el aseo y manejan los desechos mediante actividades de reciclaje, reducción y reutilización.</p>

						</li>

					</ol>

				</div>

			</div>

		</div>

		<div class="container container-sm">

			<h3 class="st-underline st-underline-secondary st-underline-thin st-green st-color-secondary st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.2s" style="animation-delay: 0.2s;">Stevia One <span class="disblk">Somos los primeros productores de stevia con el sello Rainforest Alliance Certified™</span></h3>

		</div>

	</div>

	





<?php include('include/newsletter.php') ?>

</section>





<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

												MIDDLE SECTION END

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->



<?php include('include/footer.php'); ?>