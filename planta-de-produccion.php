<?php include('include/header.php'); ?>



<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

												MIDDLE SECTION

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<section class="st-header-area st-header-1" style="background-image:url('images/planta-produccion3-1.jpg')">

	<div class="container">

		<div class="st-tbl">

			<div class="st-tbl-cell">

				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Planta de Producción</h1>

			</div>

		</div>

	</div>

</section>

<section class="st-middle-sec">

	<div class="st-common-sec st-planta-prod-sec">

		<div class="container">

			<div class="st-nuestraBx">

                <div class="plantaBx">                	
            		<h2 class="st-bot-margin-30">¡Sin químicos!<br>¡Sin alcohol! </h2>
            		<p class="st-bot-margin-30">Hemos desarrollado un proceso único de producción que utiliza solo AGUA para la extracción y purificación de los glicósidos de steviol desde la hoja stevia hasta el producto final.</p>
            		<h3 class="st-color-secondary">¡Cuidamos el medio ambiente, SOLO USAMOS AGUA!</h3>                	
                </div>

            </div>

		</div>

	</div>



    <div class="st-common-sec st-info-sec st-planta-prod-icons">
        <div class="container st-nuestraBx">
              <p>La stevia se somete a 3 procesos:</p>
              <div class="row">
                <div class="col-xs-4">
                    <div class="st-planta-icon-box">
                        <span class="st-planta-icon st-color-secondary"><i class="sicon-plants"></i></span>
                        <h4 class="st-color-secondary">EXTRACCIÓN</h4>
                    </div>
             </div>
             <div class="col-xs-4">
                <div class="st-planta-icon-box">
                    <span class="st-planta-icon st-color-secondary"><i class="sicon-ecology"></i></span>
                    <h4 class="st-color-secondary st-color-secondary">PURIFICACIÓN</h4>
                </div>
             </div>
             <div class="col-xs-4">
                <div class="st-planta-icon-box">
                   <span class="st-planta-icon st-color-secondary"><i class="sicon-secado"></i></span>
                   <h4 class="st-color-secondary st-color-secondary">SECADO</h4>
               </div>
             </div>
         </div>           
     </div>

        

    </div>

    <div class="st-common-sec st-planta-list-sec">
        <div class="container">

            <div class="row">
                <div class="col-sm-6">
                    <ul class="st-bullet-list st-bullet-list-plain st-bullet-space">

                        <li>El único elemento además de las hojas secas de stevia que utilizamos durante la producción es agua, con altos niveles de pureza, que además se reutiliza en el proceso.</li>

                        <li>Este es un proceso limpio y seguro, donde no se utiliza alcohol durante la producción. Por lo tanto, se minimiza cualquier riesgo de explosión en la planta.</li>

                    </ul>

                </div>
                <div class="col-sm-6">                    
                    <ul class="st-bullet-list st-bullet-list-plain st-bullet-space">

                        <li>El extracto de stevia nunca entra en contacto con químicos ni alcohol.</li>

                        <li>Solo utilizamos químicos para la limpieza de los equipos, y son cuidadosamente tratados a la salida del proceso de limpieza.</li>

                    </ul>

                </div>

            </div>

        </div>
    </div>

    
    
    <div class="st-common-sec st-why-stevia-sec st-planta-why-stevia">        
        <div class="container container-lg">    
            <div class="st-nuestraBx">
                <div class="plantaBx">                                        
                    <h2>Our production plant</h2>
                    <p class="st-bot-margin-30">Our Production plant will be ready on April 2017 and is located in Paita, next to the Paita Port, one of the most important ports in Peru.</p>                        
                </div>
            </div>  
            <div class="st-planta-icons-row">
                <h2 class="st-color-secondary">Why stevia in peru?</h2>
                <div class="row st-stevia-in-peru-sec">
                    <div class="col-sm-6">
                        <div class="st-icon-thumb">
                            <i class="sicon-political"></i>
                        </div>
                        <h3 class="st-color-secondary">Economy & political</h3>
                        <ul class="st-bullet-list st-bullet-list-plain st-bullet-space">
                            <li>30 years of Political Stability.</li>
                            <li>18 years of Economic Stability.</li>
                            <li>Continuity in Economic oilicies: No limitations for internacional investment, strong support for forestry and agricultural investments, liberal competition legislation.</li>
                        </ul>
                    </div>
                    <div class="col-sm-5 col-sm-offset-1">
                        <div class="st-icon-thumb">
                            <i class="sicon-gdp-growth"></i>
                        </div>
                        <h3 class="st-color-secondary">GDP</h3>
                        <p>The GDP of Peru has exerienced significant rates of growth:</p>
                        <ul class="st-bullet-list st-bullet-list-plain st-bullet-space st-bullet-list-col">
                            <li>2010 -> 8.5%</li>
                            <li>2010 -> 8.5%</li>
                            <li>2010 -> 8.5%</li>
                            <li>2013 -> 5.8%</li>
                            <li>2013 -> 5.8%</li>
                            <li>2013 -> 5.8%</li>
                        </ul>
                    </div>
                </div>
                <div class="row st-stevia-in-peru-sec">                
                    <div class="col-sm-6">
                        <div class="st-icon-thumb">
                            <i class="sicon-idea-agricultural"></i>
                        </div>
                        <h3 class="st-underline-space-15">Ideal agricultural conditions</h3>
                        <p>Vast extensions of land: the project can grow extensively with ideal agricultural conditions. Good availability of water, altitud, temperature, sunlight, with year-round harvest capacity.</p>                    
                    </div>
                    <div class="col-sm-5 col-sm-offset-1">
                        <div class="st-icon-thumb">
                            <i class="sicon-social-care"></i>
                        </div>
                        <h3 class="st-underline-space-15">Social impact</h3>
                        <ul class="st-bullet-list st-bullet-list-plain st-bullet-space">
                            <li>To create 1,200 jobs.</li>
                            <li>Provide utilities (energy, water).</li>
                            <li>Sustainable and responsible agriculture operations.</li>                        
                        </ul>                    
                    </div>
                </div>
            </div>      
        </div>
    </div>
	<div class="st-common-sec st-testimonials-sec st-planta-testmonials-sec">

		<div class="container container-lg">
                <blockquote>
                    <p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s">“Me siento muy orgulloso de trabajar en esta compañía.<br>Stevia One está desarrollando un producto que tiene un valor incalculable para la salud y el bienestar del ser humano.</p>

                    <p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.1s">Estar involucrado en la implementación de los procesos para producirlo, en condiciones tan favorables para el medio ambiente, es lo mejor que puede pasarme”.</p>

                    <div class="st-author-info st-author-info-1 st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.2s" style="animation-delay: 0.2s;">

                        <h5>Simon Vanmechelen</h5>

                        <span class="st-author-post">Jefe de Investigación y Desarrollo</span>

                    </div>

                </blockquote>
		</div>

	</div>

	<div class="st-common-sec st-info-sec st-info-sec-1 st-planta-info-sec">

		<div class="container container-lg">
			<h3 class="st-green st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.2s"><span class="disblk">Nuestros trabajadores están con nosotros desde hace años. Conocemos a cada uno. </span>Hoy, todos forman parte de la familia Stevia One.</h3>

		</div>

	</div>





<div class="st-newsletter-hidden"><?php include('include/newsletter.php') ?></div>

</section>





<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

												MIDDLE SECTION END

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->



<?php include('include/footer.php'); ?>