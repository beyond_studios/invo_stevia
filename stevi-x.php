<?php
include('include/header.php');
?>
<section class="st-header-area st-header-1" style="background-image: url('images/background-productos.png')">
	<div class="container">
		<div class="st-tbl">
			<div class="st-tbl-cell">
				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Productos</h1>
			</div>
		</div>
	</div>
</section>
<section class="st-middle-sec">
	<section class="container title">
		<div class="row">
			<div class="col-xs-12">
				<h2>STEVI-X</h2>
			</div>
		</div>
	</section>
	<section class="container body-content">
		<div class="row">
			<div class="col-xs-12">
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<h3>Soluciones a base de extracto de stevia</h3>
						<p>Stevi-x es nuestro producto de soluciones edulcorantes totalmente naturales basado en nuestro extracto de stevia natural stevi-o®. Cada aplicación es única en su sabor y dulzura. Es por eso que tenemos nuestro equipo de tecnólogos de aplicaciones a su servicio para desarrollar su solución específica de edulcorante natural, creando la combinación perfecta entre su aplicación y nuestro extracto natural de stevia.</p>
						<h3>Características:</h3>
						<ul>
							<li>Potente dulzor y reducción de calorías</li>
							<li>Idéntico perfil de sabor y sensación en boca</li>
							<li>Granulado, para facilitar la aplicación</li>
							<li>Empaques adecuados, para el tamaño de su lote de producción </li>
							<li>100% natural</li>
						</ul>
						<h3>Aplicaciones:</h3>
						<p>Como edulcorante natural para productos alimenticios y bebidas.</p>
					</div>
					<div class="col-xs-12 col-sm-6">
						<img src="images/stevi-x/stevix-big.png" title="Stevi-O" class="hidden-xs" />
						<i class="leaf hidden-xs hidden-sm hidden-md"></i>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p class="buttons"><a href="contactenos.php" class="button">CONTÁCTENOS</a></p>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>
<?php
include('include/footer.php');