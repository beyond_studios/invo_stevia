<?php include('include/header.php'); ?>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area st-header-1" style="background-image:url('../images/somos-stevia-one-bg-1.jpg')">
    <div class="container">
        <div class="st-tbl">
            <div class="st-tbl-cell">
                <h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">WE ARE STEVIA ONE</h1>
            </div>
        </div>
    </div>
</section>
<section class="st-middle-sec">

    <div class="st-common-sec st-full-bg-columns st-full-bg-columns-1">
        <div class="container">
            <div class="row st-tbl-row">
                <div class="col-sm-6" id="mision">
                    <div class="st-full-bg-col-in">
                        <h2 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">mission</h2>
                        <p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">Our only purpose: Inspire the Planet!<br><br>
Stevia One plans to be a development model for people, planet and the global economy. We want to inspire others to build a better world, and generate value for everyone: consumers, workers, shareholders, suppliers, the community and the planet.</p>
                    </div>
                </div>
                <div class="col-sm-6" id="vision">
                    <div class="st-full-bg-col-in">
                        <h2 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">vision</h2>
                        <p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">We are an organization of people motivated by happiness that will lead Stevia One to be global leaders, with a sustainable and responsible production process, impacting positively in people’s life and planet’s health.</p>
                    </div>
                </div>
            </div>          
        </div>
    </div>
    <div class="st-common-sec st-centered-image-sec st-valores-sec" id="valores">
        <div class="container">
            <h2 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Values</h2>
            <ul class="st-bullet-list st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
                <li>We are honest and transparent. </li>
                <li>Happiness. We are passionate in what we do.</li>
                <li>Sustainability is everything.</li>
                <li>We are creative, original and innovators.</li>
                <li>Cooperative communication. We listen and learn from others. </li>
                <li>Operational Excellence.</li>
                <li>We deliver results without excuses.</li>

            </ul>
        </div>
    </div>

    <div class="st-common-sec st-info-sec st-info-sec-1">
        <div class="container masqueBx">
            <h3 class="st-green st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.2s" style="animation-delay: 0.2s;">Stevia One,<br>We want to Inspire the Planet!</h3>
        </div>
    </div>
    

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                    NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->


    <div class="st-newsletter-hidden"><?php include('include/newsletter.php') ?></div>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</section>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<?php include('include/footer.php'); ?>