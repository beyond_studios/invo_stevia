<?php include('include/header.php'); ?>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area st-header-1" style="background-image:url('../images/somos-bg.jpg')">
	<div class="container">
		<div class="st-tbl">
			<div class="st-tbl-cell">
				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Manifest</h1>
			</div>
		</div>
	</div>
</section>
<section class="st-middle-sec">
	<div class="st-common-sec">
		<div class="container container-md st-md-content st-manifesto-content">
			<p>Can a sweet stevia leaf change the life of millions of people?</p>
			<p>Can a small plant like stevia revolutionize the food industry?</p>
			<p><strong>Yes. In Stevia One, we are sure the answer is yes.</strong></p>
			<p>Can a Company be responsible, conscious and sustainable?</p>
			<p>Can a Company make a positive impact in the planet?</p>
			<p><strong>In Stevia One, we believe we are capable of that and more. We are revolutionizing the way to make business.</strong></p>
		</div>
	</div>
	<div class="st-big-image-panel st-big-image-panel-1" style="background-image:url('../images/manifiesto-inner-bg-1.jpg')">
		<div class="container">
			<p class="st-small-txt"><span>This is</span> our Manifest:</p>
			<p>In Stevia One, <br>
				We believe in what we do.<br>
				When you do what you believe in: you Inspire.<br>
				And when you inspire: your people commit.</p>
			<p>Stevia One, our unique purpose: <br>
				Inspire the Planet!</p>
		</div>
	</div>
	




<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->


	<?php //include('include/newsletter.php') ?>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</section>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<?php include('include/footer.php'); ?>