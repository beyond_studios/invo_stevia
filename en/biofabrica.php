<?php include('include/header.php'); ?>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area st-header-1" style="background-image:url('../images/biofabrica-bg-1.jpg')">
	<div class="container">
		<div class="st-tbl">
			<div class="st-tbl-cell">
				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Bioplant</h1>
			</div>
		</div>
	</div>
</section>
<section class="st-middle-sec">
	<div class="st-common-sec st-biofabrica-sec">
		<div class="container container-lg">
			<div class="st-biofabricaBx">
				<h2>Bioplant</h2>
				<div class="row">
					<div class="col-md-6">
						<h3 class="st-small-heading st-bot-margin-30">The work done in our laboratories is very important for the genetic vegetable improvement.</h3>
					</div>
					<div class="col-md-6">
						<ul class="st-bullet-list st-bullet-space">
							<li>Allows the exponential multiplication of the high genetic vigor individuals, free from plagues and diseases.</li>
							<li>The bioplant have a germplasm bank that allows us to preserve a back-up of our diversity, as well as external diversity for research.</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="st-common-sec st-info-sec st-testimonials-sec st-biofabrica-testimonial-sec">
		<div class="container masqueBx">
			<blockquote>
				<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;">“I work at Stevia One since last year, and thank each opportunity given to me, they help me discover new potentials on me. I’m sure that the stevia will help the world to have a better quality of life in the future”.</p>
				<div class="st-author-info st-author-info-1 st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.2s" style="animation-delay: 0.2s;">
					<h5>Luz Marina Díaz Rodríguez</h5>
					<span class="st-author-post">Bioplant Technician</span>
				</div>
			</blockquote>
		</div>
	</div>
	




<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->


	<div class="st-newsletter-hidden"><?php include('include/newsletter.php') ?></div>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</section>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<?php include('include/footer.php'); ?>