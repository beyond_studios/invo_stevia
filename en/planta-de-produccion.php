<?php include('include/header.php'); ?>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area st-header-1" style="background-image:url('../images/planta-produccion3-1.jpg')">
	<div class="container">
		<div class="st-tbl">
			<div class="st-tbl-cell">
				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Production Plant</h1>
			</div>
		</div>
	</div>
</section>
<section class="st-middle-sec">
	<div class="st-common-sec st-planta-prod-sec">
		<div class="container">
			<div class="st-nuestraBx">
                <div class="plantaBx">
                  <h2 class="st-bot-margin-30">No chemicals!<br>No alcohol! </h2>
                  <p class="st-bot-margin-30">We have developed a unique process of production that uses only WATER for the extraction and purification of the glycosides of the steviol from the stevia leaves to the final product.</p>
                  <h3 class="st-color-secondary">We care about the environment, WE ONLY USE WATER!</h3>                	
              </div>
            </div>
		</div>
	</div>

    <div class="st-common-sec st-info-sec st-planta-prod-icons">
        <div class="container st-nuestraBx">
            <p>The stevia is subject to 3 processes:</p>
            <div class="row">
                <div class="col-xs-4">
                    <div class="st-planta-icon-box">
                        <span class="st-planta-icon st-color-secondary"><i class="sicon-plants"></i></span>
                        <h4 class="st-color-secondary">EXTRACTION</h4>
                     </div>
               </div>
               <div class="col-xs-4">
                    <div class="st-planta-icon-box">
                        <span class="st-planta-icon st-color-secondary"><i class="sicon-ecology"></i></span>
                         <h4 class="st-color-secondary">PURIFICATION</h4>
                     </div>
               </div>
               <div class="col-xs-4">
                    <div class="st-planta-icon-box">
                        <span class="st-planta-icon st-color-secondary"><i class="sicon-secado"></i></span>
                        <h4 class="st-color-secondary">DRYING</h4>
                     </div>
               </div>
           </div>
       </div>
        
    </div>
    <div class="st-common-sec st-planta-list-sec">
        <div class="container">            
                <div class="row">
                    <div class="col-sm-6">
                        <ul class="st-bullet-list st-bullet-list-plain st-bullet-space">
                            <li>We use for the extraction process only dry leaves of stevia and water, with high purity levels, that is reused during the process.</li>
                            <li>This is a clean and secure process, where no alcohol is used during the production;therefore, any risk of plant explosion is minimized.</li>
                        </ul>
                    </div>                    
                    <div class="col-sm-6">                        
                        <ul class="st-bullet-list st-bullet-list-plain st-bullet-space">
                            <li>The extract of stevia is never in contact with chemicals or alcohol.</li>
                            <li>We only use chemicals for the cleaning of equipment, and they are carefully treated during the end of the cleaning process.</li>
                        </ul>
                    </div>
                </div>
            
        </div>
    </div>
    <div class="st-common-sec st-why-stevia-sec st-planta-why-stevia">        
        <div class="container container-lg">   
            <div class="st-nuestraBx">
                <div class="plantaBx">
                    <h2>Our production plant</h2>
                    <p class="st-bot-margin-30">Our Production plant will be ready on April 2017 and is located in Paita, next to the Paita Port, one of the most important ports in Peru.</p>         
                </div>
            </div>   
            <div class="st-planta-icons-row">      
                <h2 class="st-color-secondary">Why stevia in peru?</h2>
                <div class="row st-stevia-in-peru-sec">
                    <div class="col-sm-6">
                        <div class="st-icon-thumb">
                            <i class="sicon-political"></i>
                        </div>
                        <h3 class="st-color-secondary">Economy & political</h3>
                        <ul class="st-bullet-list st-bullet-list-plain st-bullet-space">
                            <li>30 years of Political Stability.</li>
                            <li>18 years of Economic Stability.</li>
                            <li>Continuity in Economic oilicies: No limitations for internacional investment, strong support for forestry and agricultural investments, liberal competition legislation.</li>
                        </ul>
                    </div>
                    <div class="col-sm-5 col-sm-offset-1">
                        <div class="st-icon-thumb">
                           <i class="sicon-gdp-growth"></i>
                        </div>
                        <h3 class="st-color-secondary">GDP</h3>
                        <p>The GDP of Peru has exerienced significant rates of growth:</p>
                        <ul class="st-bullet-list st-bullet-list-plain st-bullet-space st-bullet-list-col">
                            <li>2010 -> 8.5%</li>
                            <li>2010 -> 8.5%</li>
                            <li>2010 -> 8.5%</li>
                            <li>2013 -> 5.8%</li>
                            <li>2013 -> 5.8%</li>
                            <li>2013 -> 5.8%</li>
                        </ul>
                    </div>
                </div>
                <div class="row st-stevia-in-peru-sec">                
                    <div class="col-sm-6">
                        <div class="st-icon-thumb">
                            <i class="sicon-idea-agricultural"></i>
                        </div>
                        <h3 class="st-underline-space-15">Ideal agricultural conditions</h3>
                        <p>Vast extensions of land: the project can grow extensively with ideal agricultural conditions. Good availability of water, altitud, temperature, sunlight, with year-round harvest capacity.</p>                    
                    </div>
                    <div class="col-sm-5 col-sm-offset-1">
                        <div class="st-icon-thumb">
                            <i class="sicon-social-care"></i>
                        </div>
                        <h3 class="st-underline-space-15">Social impact</h3>
                        <ul class="st-bullet-list st-bullet-list-plain st-bullet-space">
                            <li>To create 1,200 jobs.</li>
                            <li>Provide utilities (energy, water).</li>
                            <li>Sustainable and responsible agriculture operations.</li>                        
                        </ul>                    
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div class="st-common-sec st-testimonials-sec st-planta-testmonials-sec">
		<div class="container container-lg">
			    <blockquote>
                    <p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s">“I feel very proud to work in this company.<br>Stevia One is developing a product that has an incaluable value for the health and wellness of the human being.</p>
                    <p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.1s">Being involved in the implementation of the processes to produce it, in favorable conditions for the environment, is the best that can happen to me”.</p>
                    <div class="st-author-info st-author-info-1 st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">
                        <h5>Simon Vanmechelen</h5>
                        <span class="st-author-post">Head of Research and Development</span>
                    </div>
                </blockquote>
            
		</div>
	</div>
	<div class="st-common-sec st-info-sec st-info-sec-1 st-planta-info-sec">
		<div class="container container-lg">
			<h3 class="st-green st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.2s"><span class="disblk">Our workers are with us for so many years. We know each of them. </span>Today, all are part of the Stevia One family.</h3>
		</div>
	</div>




<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->


	<div class="st-newsletter-hidden"><?php include('include/newsletter.php') ?></div>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</section>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<?php include('include/footer.php'); ?>