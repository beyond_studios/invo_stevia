<?php
include('include/header.php');
?>
<section class="st-header-area st-header-1" style="background-image: url('../images/background-productos.png')">
	<div class="container">
		<div class="st-tbl">
			<div class="st-tbl-cell">
				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Products</h1>
			</div>
		</div>
	</div>
</section>
<section class="st-middle-sec">
	<section class="container title">
		<div class="row">
			<div class="col-xs-12">
				<h2>STEVI-X</h2>
			</div>
		</div>
	</section>
	<section class="container body-content">
		<div class="row">
			<div class="col-xs-12">
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<h3>Solutions based on stevia extract</h3>
						<p>Stevi-x is our product of all-natural solutions based on our stevia-o® natural stevia extract. Each application is unique in its taste and sweetness. That's why we have our team of application technologies for your service to develop your specific natural sweetener solution, creating the perfect combination between your application and our natural stevia extract.</p>
						<h3>Products Features:</h3>
						<ul>
							<li>Powerful sweetness and reduction of calories.</li>
							<li>Identical profile of taste and mouthfeel.</li>
							<li>Granulated, to facilitate the application.</li>
							<li>Suitable packaging, for the size of its production lot.</li>
							<li>100% natural</li>
						</ul>
						<h3>Applications:</h3>
						<p>As a natural sweetener for food products and beverages.</p>
					</div>
					<div class="col-xs-12 col-sm-6">
						<img src="../images/stevi-x/stevix-big.png" title="Stevi-O" class="hidden-xs" />
						<i class="leaf hidden-xs hidden-sm hidden-md"></i>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p class="buttons"><a href="contact-us.php" class="button">Contact Us</a></p>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>
<?php
include('include/footer.php'); ?>