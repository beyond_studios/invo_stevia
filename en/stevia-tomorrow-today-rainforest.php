<?php include('include/header.php'); ?>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
					MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area st-header-2" style="background-image:url('../images/rainforest_1-1.jpg')">
	<div class="container">
		<div class="st-tbl">
			<div class="st-tbl-cell">
				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0">Rainforest<br>
				Alliance Certified™<small>We offer the first and only stevia product</small></h1>

			</div>
		</div>
		<a href="#st-middle-content" class="st-header-link st-smooth-scroll st-animate" data-os-animation="fadeInDown" data-os-animation-delay="0"><i class="sicon-arrow-l-down"></i></a>
	</div>
</section>
<section class="st-middle-sec" id="st-middle-content">
	
	<div class="st-common-sec st-round-icon-sec st-rainforest-sec">
		<div class="container">
			<div class="row">
			  <div class="st-tbl-row st-rounded-icon-row">
				<div class="col-sm-3">
					<div class="st-rounded-icon-box st-rounded-icon-box-lime-green st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0">
						<div class="st-rounded-icon-box-in">
							<span class="st-rounded-icon-wrap">
								<img src="../images/rainforest-1.svg" alt="Rainforest Alliance Certified">
							</span>							
						</div>
					</div>
				</div>
				<div class="col-sm-9">
					<h2 class="st-small-desc st-rainforest-heading st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">We offer the first and only stevia product to carry the <strong>Rainforest Alliance Certified™</strong> seal</h2>
					<p class="st-round-icon-desc st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.5s">We source stevia from fields where the water, soil and the ecosystem are preserved, the workers are fairly treated and the local communities benefit.</p>

				</div>
			</div>
			</div>
		</div>
	</div>
	
	<div class="st-common-sec st-info-sec st-rainforest-info-sec">
		<div class="container container-lg">
			<div class="st-biofabricaBx">
				<div class="trazablesBx">
					<h2 class="st-color-secondary">Rainforest Alliance Certified™</h2>
						<p>After a lot of effort and continuous work, Stevia One is proud to have the first stevia products to carry the Rainforest Alliance Certified™ seal. Rainforest Alliance certification means that our stevia plantation meets comprehensive standards for sustainable agriculture that protects wildlands, waterways, wildlife habitat and the rights and well-being of workers, their families and communities.</p>
					
						<p class="st-bot-margin-30">The Rainforest Alliance Certified™ seal is recognized by consumers around the world as the symbol of environmental, social and economic sustainability. Certification is awarded to farms that have met the comprehensive standard of the Sustainable Agriculture Network (SAN). </p>
				</div>
			</div>
		</div>
	</div>

	<div class="st-common-sec st-round-icon-sec st-rainforest-listings">
		<div class="container rainBx">
			<h5>The SAN standard is based in <strong>10 principles:</strong></h5>			
			<ol class="st-ol-listing st-ol-listing-plain st-bullet-space st-listing-twocol" start="0">
				<li><h4>Management system</h4>
					<p>Certified farms operate an environmental and social management system according to the complexity of their operations and in conformance with applicable local laws. The farms record their energy consumption, to make an effort to reduce it and use more renewable energy.</p>
				</li>
				<li><h4>Occupational health</h4>
					<p>Certified farms have occupational health and safety programs to reduce the risk of accidents and to support the health of workers exposed to hazardous activities such as the operation of machinery or the application of authorized pesticides.</p>
				</li>
				<li><h4>Ecosystem conservation</h4>
					<p>Farmers must conserve existing ecosystems and aid in the restoration of critical areas. They achieve this by taking steps to protect waterways and wetlands from erosion and contamination, prohibiting logging and other deforestation, and maintaining vegetation barriers.</p>
				</li>
				<li><h4>Community relations</h4>
					<p>Certified farms are good neighbors in their communities. They inform the surrounding communities and local stakeholder groups about their activities and plans. They discuss the potential impacts of their activities and contribute to local development through employment, training and public works.</p>
				</li>
				<li><h4>Wildlife protection</h4>
					<p>The hunting of wild animals is prohibited on certified farms, as is the keeping of wild animals in captivity and the extraction of wild plants.</p>
				</li>
				<li><h4>Soil conservation</h4>
					<p>The SAN seeks the improvement of soils over the long term, which is why certified farms use organic fertilizers, propagate ground cover vegetation and plant natural barriers to reduce erosion and runoff of sediments to rivers, streams and lakes. Fire is never used to prepare new plots for production.</p>
				</li>
				<li><h4>Water conservation</h4>
					<p>Certified farms conduct activities to conserve water and avoid wasting this resource. Farms prevent contamination of surface and underground water by treating and monitoring waste-water.</p>
				</li>
				<li><h4>Integrated crop management</h4>
					<p>Certified farms monitor crop pests and use biological or mechanical methods for pest control as a first choice. If the pests cause considerable economic damage, certain pesticides can be used but only by applying them using every possible safeguard for the protection of the workers, the communities and the environment. Transgenic crops (genetically modified organisms) are not grown or introduced on certified farms.</p>
				</li>
				<li><h4>Working conditions</h4>
					<p>Farm employees are treated with respect and in accordance with the main conventions of the International Labor Organization. They are paid a legal minimum wage and have access to education and healthcare services. Minors under 15 years of age or the age established by local law are not contracted.</p>
				</li>
				<li><h4>Integrated Waste Management</h4>
					<p>Certified farms are aware of the types and quantities of wastes they generate. Wastes are managed through programs for recycling, reducing consumption and reuse. Wastes are sorted, treated and disposed of in ways that minimize environmental and health impacts for the workers and the communities. Farms are aware of their greenhouse gas emissions and how to reduce their carbon footprint.</p>
				</li>
			</ol>
				
		</div>
	</div>

	<div class="st-common-sec st-medal-sec">
		<div class="container">
			<div class="st-medal-icon-bg"><i class="sicon-medal"></i></div>
			<div class="st-medal-icon"><i class="sicon-medal"></i></div>
			<h3 class="st-color-secondary st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">Stevia One <span class="disblk">We are the first producers of stevia with the seal Rainforest Alliance Certified™</span></h3>
		</div>
	</div>

	<div class="st-common-sec st-info-sec st-testimonials-sec">
		<div class="container container-sm">
			
				<blockquote>
					<p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">“Stevia One has changed the life of the people involved in this project. More than a job or a fixed income. People from the field are proud to have a fair job within a responsible enterprise”.</p>
					<div class="st-author-info st-author-info-1 st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">
					<h5>Julia Aredo </h5>
					<span class="st-author-post">Field Manager Fundo Calzada</span>
					<span class="st-author-company">Agronomist</span>
								</div>
				</blockquote>
			
		</div>
	</div>
	

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->


	<div class="st-newsletter-hidden"><?php include('include/newsletter.php') ?></div>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</section>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<?php include('include/footer.php'); ?>