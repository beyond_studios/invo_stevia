<?php include('include/header.php'); ?>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
					MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area st-header-2 st-stevia-tomorrow-header" style="background-image:url('../images/stevia-tomorrow-today-bg-1.jpg')">
	<div class="container">
		<div class="st-tbl">
			<div class="st-tbl-cell">
				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0"><span class="st-heading-leaf-1 st-animate"  data-os-animation="fadeInUp" data-os-animation-delay="0.5s"><img src="../images/stevia-tomorrow-today-leaf-1.png" alt="Stevia Leaf" /></span>No chemicals!<br>No alcohol! <small>
We only use water</small><span class="st-heading-leaf-2 st-animate"  data-os-animation="fadeInUp" data-os-animation-delay="0.8s"><img src="../images/stevia-tomorrow-today-leaf-2.png" alt="Stevia Leaf" /></span></h1>

			</div>
		</div>
		<a href="#st-middle-content" class="st-header-link st-smooth-scroll st-animate" data-os-animation="fadeInDown" data-os-animation-delay="0"><i class="sicon-arrow-l-down"></i></a>
	</div>
</section>
<section class="st-middle-sec" id="st-middle-content">
	
	<div class="st-common-sec st-tomorrow-today-desc-sec st-round-icon-sec">
		<div class="container">
			<div class="stevia-tomorrow-today-leaf-3 st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.5"><img src="../images/stevia-tomorrow-today-leaf-3.png" alt="Stevia Leaf" /></div>
			<div class="stevia-tomorrow-today-leaf-4 st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.8"><img src="../images/stevia-tomorrow-today-leaf-4.png" alt="Stevia Leaf" /></div>
			<div class="row">
			  <div class="st-tbl-row st-rounded-icon-row">
				<div class="col-sm-2">
					<div class="st-rounded-icon-box st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0">
						<div class="st-rounded-icon-box-in">
							<span class="st-rounded-icon-wrap">
								<i class="sicon-water-2"></i>
							</span>							
						</div>
					</div>
				</div>
				<div class="col-sm-10">
					<h2 class="st-small-desc st-underline st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">We have developed a unique extraction and purification process to convert our stevia leaves to final product.<br>We care about the environment, <br><strong>WE ONLY USE WATER!</strong></h2>

				</div>
			</div>
			</div>
		</div>
	</div>
	
	<div class="st-common-sec st-info-sec  st-tomorrow-today-icons-list">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div class="st-tomorrow-today-icon st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s"><i class="sicon-factory"></i></div>
					<p class="st-info-block st-animate"  data-os-animation="fadeInUp" data-os-animation-delay="0.2s">
						We care about <br>
						the environment
					</p>
				</div>
				<div class="col-sm-4">
					<div class="st-tomorrow-today-icon st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s"><i class="sicon-certificate"></i></div>
					<p class="st-info-block st-animate"  data-os-animation="fadeInUp" data-os-animation-delay="0.6s">
						Our acqueous process is<br>
						saffer for our workers
					</p>
				</div>
				<div class="col-sm-4">
					<div class="st-tomorrow-today-icon st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.4s"><i class="sicon-water-1"></i></div>
					<p class="st-info-block st-animate"  data-os-animation="fadeInUp" data-os-animation-delay="0.8s">
						We only use water for<br>
						the extraction process
					</p>
				</div>
			</div>
		</div>
	</div>
	

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->


	<div class="st-newsletter-hidden"><?php include('include/newsletter.php') ?></div>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</section>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<?php include('include/footer.php'); ?>