<?php include('include/header.php'); ?>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area" style="background-image:url('../images/port-application-bg.jpg')">
	<div class="container">
		<div class="st-tbl">
			<div class="st-tbl-cell">
				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Applications<small></small></h1>
			</div>
		</div>
	</div>
</section>
<section class="st-middle-sec">
	<div class="st-common-sec st-theme-sec">
		<div class="container">
			<div class="st-nuestraBx">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque aliquet elementum tempor. Cras mollis gravida neque, a accumsan neque iaculis in. </p>
				<p class="st-underline st-underline-primary">Vivamus a dignissim arcu. In tristique arcu diam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Duis a metus aliquet, volutpat urna ac, semper odio. </p>
			</div>
		</div>
	</div>
	<div class="st-common-sec st-info-sec">
		<div class="container container-lg">
			<div class="row port-application">
				<div class="col-sm-3">
					<i class="sicon-glass"></i>
					<h3>Lorem Ipsum</h3>
					<p>Quisque aliquet elementum tempor. Cras mollis gravida neque, a accumsan neque iaculis in. Vivamus a dignissim arcu.</p>
				</div>
				<div class="col-sm-3">
					<i class="sicon-globe"></i>
					<h3>Natural Amet</h3>
					<p>Cras mollis gravida neque, a accumsan neque iaculis in. Vivamus a dignissim arcu.</p>
				</div>
				<div class="col-sm-3">
					<i class="sicon-face"></i>
					<h3>Caries Ipsum</h3>
					<p>A accumsan neque iaculis in. Vivamus a dignissim ras mollis gravida neque.</p>
				</div>
				<div class="col-sm-3">
					<i class="sicon-products"></i>
					<h3>Products</h3>
					<p>Quisque aliquet elementum tempor. Cras mollis gravida neque, a accumsan neque iaculis in. Vivamus a dignissim arcu.</p>
				</div>
			</div>
			<hr class="st-hr-space">
			<p class="text-center">
				<a href="#" class="xtra-lg-btn"><i class="sicon-products btnIcon"></i><span>ver nuestros<br>productos</span><i class="sicon-arrow-s-right btnArrow"></i></a>
			</p>
		</div>
	</div>
	




<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->


	<?php include('include/newsletter.php') ?>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</section>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<?php include('include/footer.php'); ?>