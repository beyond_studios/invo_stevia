<?php include('include/header.php'); ?>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area" style="background-image:url('../images/contact-bg.jpg')">
    <div class="container">
        <div class="st-tbl">
            <div class="st-tbl-cell">
                <h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Our leaders<small></small></h1>
            </div>
        </div>
    </div>
</section>
<section class="st-middle-sec">
    <div class="st-common-sec st-theme-sec st-profile-sec">
        <div class="container">
            <div class="row st-profile-row">
                <div class="col-sm-4">
                    <span class="st-profile-pic-wrap st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0">
                        <img src="../images/profile-1.jpg" alt="Marc Saverys" class="img-responsive" draggable="false">
                    </span>
                </div>
                <div class="col-sm-8">
                    <blockquote>
                        <p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">“I feel enthusiastic to be part of this project. Our commitment to the planet through Stevia One is to be part of a solution for two major health problems of the 21st century: obesity and diabetes. We will contribute to the Peruvian economy development, motivating our stakeholders to be the Stevia market leaders. <br>
                        We will make our people proud to be part of the Stevia One Family.”</p>
                        <div class="st-author-info st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">
                          <h5>Marc Saverys</h5>
                          <span class="st-author-post">President of the Board</span>
                        </div>
                    </blockquote>
                </div>
            </div>
        </div>
    </div>

    <div class="st-common-sec st-full-bg-columns">
        <div class="container-fluid">
            <div class="row st-tbl-row">
                <div class="col-sm-6">
                    <div class="st-full-bg-col-in">                        
                        <div class="row st-profile-row">
                            <div class="col-sm-4 st-small-profile-col">
                                <span class="st-profile-pic-wrap st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0">
                                    <img src="../images/profile-6.jpg" alt="Filip Balcaen" class="img-responsive" draggable="false">
                                </span>
                                <div class="st-author-info st-author-info-bottom st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">
                                  <h5>Sebastiaan Saverys</h5>
                                  <span class="st-author-post">Executive Director</span>                                   
                                </div>
                            </div>
                            <div class="col-sm-8 st-small-profile-desc-col">
                                <blockquote>
                                    <p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">“I always asked myself, what I can do to better the world. Today, I know that we can do it in an efficient way with Stevia One.<br>
                                    I’m convinced that as good producers and sellers of stevia, we can help to solve a global health problem. And if each day, we can inspire somebody, We can inspire the planet!”</p>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 st-bg-gray">
                    <div class="st-full-bg-col-in">                        
                        <div class="row st-profile-row">
                            <div class="col-sm-4 st-small-profile-col">
                                <span class="st-profile-pic-wrap st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0">
                                    <img src="../images/profile-2.jpg" alt="Sebastiaan Saverys" class="img-responsive" draggable="false">
                                </span>
                                <div class="st-author-info st-author-info-bottom st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">
                                  <h5>Filip Balcaen</h5>
                                  <span class="st-author-post">Board Member</span>
                                </div>
                            </div>
                            <div class="col-sm-8 st-small-profile-desc-col">
                                <blockquote>
                                    <p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">“I am excited to support Stevia One to become the reference in the stevia industry for the product quality and vertically integrated and traceable production based on sustainable technologies. I strongly believe that healthy lifestyle is relevant to fight obesity and diabetes and through Stevia One we are able to provide the solution to a more balanced diet.”</p>
                                </blockquote>                                    
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row st-tbl-row st-tbl-row-invert">                
                <div class="col-sm-6">
                    <div class="st-full-bg-col-in">                        
                        <div class="row st-profile-row">
                            <div class="col-sm-4 st-small-profile-col">
                                <span class="st-profile-pic-wrap st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0">
                                    <img src="../images/profile-3.jpg" alt="Augusto Baertl" class="img-responsive" draggable="false">
                                </span>
                                <div class="st-author-info st-author-info-bottom  st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">
                                  <h5>Augusto Baertl</h5>
                                  <span class="st-author-post">Board Member</span>                                   
                                </div>
                            </div>
                            <div class="col-sm-8 st-small-profile-desc-col">
                                <blockquote>
                                    <p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">“I feel exited to contribute with a team that demonstrate what building the basis of future enterprise really is. I feel committed with this project that benefits the regions we operate within Peru.”</p>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 st-bg-theme">
                    <div class="st-full-bg-col-in">                        
                        <div class="row st-profile-row">
                            <div class="col-sm-4 st-small-profile-col">
                                <span class="st-profile-pic-wrap st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0">
                                    <img src="../images/profile-7.jpg" alt="Gilles Plaquet" class="img-responsive" draggable="false">
                                </span>
                                <div class="st-author-info st-author-info-bottom st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">
                                  <h5>Gilles Plaquet</h5>
                                  <span class="st-author-post">Board Member</span>                                   
                                </div>
                            </div>
                            <div class="col-sm-8 st-small-profile-desc-col">
                                <blockquote>
                                    <p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">“I feel that I am part of a strong team, success is down the road. I am sure that Stevia One will contribute to the world fighting against obesity and diabetes, this is the challenge for the “millennials”. Stevia One offers a natural and effective solution to continue enjoying the sweetening of life.”</p>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row st-tbl-row">
                <div class="col-sm-6">
                    <div class="st-full-bg-col-in">                        
                        <div class="row st-profile-row">
                            <div class="col-sm-4 st-small-profile-col">
                                <span class="st-profile-pic-wrap st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0">
                                    <img src="../images/profile-4.jpg" alt="Leslie Pierce Diez Canseco" class="img-responsive" draggable="false">
                                </span>
                                <div class="st-author-info st-author-info-bottom st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">
                                  <h5>Leslie Pierce Diez Canseco</h5>
                                  <span class="st-author-post">Board Member</span>                                  
                                </div>
                            </div>
                            <div class="col-sm-8 st-small-profile-desc-col">
                                <blockquote>
                                    <p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">“I feel committed and proud to be a part of a project as Stevia One, which main purpose is to contribute to the health of people and well-being of their environment. I’m convinced that, in the near future, this company will dictate the guidelines of what to be socially responsible means taking care of the health of the world. Today, I feel part of a dream that will do good to our planet.”</p>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 st-bg-gray">
                    <div class="st-full-bg-col-in">                        
                        <div class="row st-profile-row">
                            <div class="col-sm-4 st-small-profile-col">
                                <span class="st-profile-pic-wrap st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0">
                                    <img src="../images/profile-5.jpg" alt="Felipe Ramos Guevara" class="img-responsive" draggable="false">
                                </span>
                                <div class="st-author-info st-author-info-bottom st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">
                                  <h5>Felipe Ramos Guevara</h5>                                  
                                  <span class="st-author-post">CEO</span>
                                </div>
                            </div>
                            <div class="col-sm-8 st-small-profile-desc-col">
                                <blockquote>
                                    <p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">“I’m proud to be back in Peru and be part of this wonderful team, in a company with such an elevated purpose as Stevia One, that worries about the people, the planet and inspire other companies to do the same.”</p>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>
    
    

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                    NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->


    <?php include('include/newsletter.php') ?>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</section>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<?php include('include/footer.php'); ?>