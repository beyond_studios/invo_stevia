<?php include('include/header.php'); ?>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area" style="background-image:url('../images/rainforest-bg.jpg')">
	<div class="container">
		<div class="st-tbl">
			<div class="st-tbl-cell">
				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Rainforest Alliance <br>Certified™<small></small></h1>
			</div>
		</div>
	</div>
</section>
<section class="st-middle-sec">
	<div class="st-common-sec st-theme-sec">
		<div class="container container-lg">
			<div class="st-biofabricaBx">
				<h2 class="st-color-secondary"><span>Rainforest<br>Alliance<br>Certified™</span> 
					<span class="st-heading-img">
						<img src="../images/rainforest.svg" alt="Rainforest Alliance Certified">
					</span>
				</h2>
				<div class="row trazablesBx">
					<div class="col-md-6 col-sm-5">
						<p>After a lot of effort and continuous work, Stevia One is proud to have the first stevia products to carry the Rainforest Alliance Certified™ seal. Rainforest Alliance certification means that our stevia plantation meets comprehensive standards for sustainable agriculture that protects wildlands, waterways, wildlife habitat and the rights and well-being of workers, their families and communities.</p>
					</div>
					<div class="col-md-1 hiddne-sm">
					</div>
					<div class="col-md-5 col-sm-7">
						<p class="st-bot-margin-30">The Rainforest Alliance Certified™ seal is recognized by consumers around the world as the symbol of environmental, social and economic sustainability. Certification is awarded to farms that have met the comprehensive standard of the Sustainable Agriculture Network (SAN). </p>
					</div>
				</div>
			</div>
		</div>
	</div>
	
    <div class="st-common-sec st-info-sec">
		<div class="container container-lg rainBx">
			<h5>The SAN standard is based in <span>10 principles:</span></h5>
			<div class="row">
				<div class="col-sm-6">
					<ol class="st-ol-listing st-bullet-space" start="0">
						<li>Management system<br>
							<p>Certified farms operate an environmental and social management system according to the complexity of their operations and in conformance with applicable local laws. The farms record their energy consumption, to make an effort to reduce it and use more renewable energy.</p>
						</li>
						<li>Ecosystem conservation<br>
							<p>Farmers must conserve existing ecosystems and aid in the restoration of critical areas. They achieve this by taking steps to protect waterways and wetlands from erosion and contamination, prohibiting logging and other deforestation, and maintaining vegetation barriers.</p>
						</li>
						<li>Wildlife protection<br>
							<p>The hunting of wild animals is prohibited on certified farms, as is the keeping of wild animals in captivity and the extraction of wild plants.</p>
						</li>
						<li>Water conservation<br>
							<p>Certified farms conduct activities to conserve water and avoid wasting this resource. Farms prevent contamination of surface and underground water by treating and monitoring waste-water.</p>
						</li>
						<li>Working conditions<br>
							<p>Farm employees are treated with respect and in accordance with the main conventions of the International Labor Organization. They are paid a legal minimum wage and have access to education and healthcare services. Minors under 15 years of age or the age established by local law are not contracted.</p>
						</li>
					</ol>
				</div>
				<div class="col-sm-6 st-padding-top-0">
					<ol class="st-ol-listing st-bullet-space" start="5">
						<li>Occupational health<br>
							<p>Certified farms have occupational health and safety programs to reduce the risk of accidents and to support the health of workers exposed to hazardous activities such as the operation of machinery or the application of authorized pesticides.</p>
						</li>
						<li>Community relations<br>
							<p>Certified farms are good neighbors in their communities. They inform the surrounding communities and local stakeholder groups about their activities and plans. They discuss the potential impacts of their activities and contribute to local development through employment, training and public works.</p>
						</li>
						<li>Soil conservation<br>
							<p>The SAN seeks the improvement of soils over the long term, which is why certified farms use organic fertilizers, propagate ground cover vegetation and plant natural barriers to reduce erosion and runoff of sediments to rivers, streams and lakes. Fire is never used to prepare new plots for production.</p>
						</li>
						<li>Integrated crop management<br>
							<p>Certified farms monitor crop pests and use biological or mechanical methods for pest control as a first choice. If the pests cause considerable economic damage, certain pesticides can be used but only by applying them using every possible safeguard for the protection of the workers, the communities and the environment. Transgenic crops (genetically modified organisms) are not grown or introduced on certified farms.</p>
						</li>
						<li>Integrated Waste Management<br>
							<p>Certified farms are aware of the types and quantities of wastes they generate. Wastes are managed through programs for recycling, reducing consumption and reuse. Wastes are sorted, treated and disposed of in ways that minimize environmental and health impacts for the workers and the communities. Farms are aware of their greenhouse gas emissions and how to reduce their carbon footprint.</p>
						</li>
					</ol>
				</div>
			</div>
		</div>
		<div class="container container-sm">
			<h3 class="st-underline st-underline-secondary st-underline-thin st-green st-color-secondary st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.2s" style="animation-delay: 0.2s;">Stevia One <span class="disblk">We are the first producers of stevia with the seal Rainforest Alliance Certified™</span></h3>
		</div>
	</div>
	




<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->


	<?php include('include/newsletter.php') ?>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</section>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<?php include('include/footer.php'); ?>