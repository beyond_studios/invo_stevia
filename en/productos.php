<?php
include('include/header.php');
?>
<section class="st-header-area st-header-1" style="background-image: url('../images/background-productos.png')">
	<div class="container">
		<div class="st-tbl">
			<div class="st-tbl-cell">
				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Products</h1>
			</div>
		</div>
	</div>
</section>
<section class="st-middle-sec">
	<div class="st-common-sec st-common-center-text">
		<div class="container">
			<p class="text-green">CALORIE REDUCTION<br/>COMPLETELY NATURAL</p>
		</div>
	</div>
	<div class="st-common-sec st-full-bg-columns st-full-bg-columns-1">
		<div class="container">
			<div class="row st-tbl-row">
				<div class="col-sm-4 color-1">
					<div class="st-full-bg-col-in">
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;"><img src="../images/productos/icon-calorie-reduction.png" /></p>
						<h2 class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;">INTENSE<br/>CALORIE<br/>REDUCTION</h2>
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.3s" style="animation-delay: 0.3s;">Experts in the application of stevia.</p>
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.3s" style="animation-delay: 0.3s;">Natural sweetener solutions to achieve the desired calorie reductions.</p>
					</div>
				</div>
				<div class="col-sm-4 color-4">
					<div class="st-full-bg-col-in">
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;"><img src="../images/productos/icon-cost-effective.png" /></p>
						<h2 class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;">COST<br/>EFFECTIVE<br/>&nbsp;</h2>
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.3s" style="animation-delay: 0.3s;">Development of customized products for their applications</p>
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.3s" style="animation-delay: 0.3s;">The suitable natural solution for you, generating considerable savings compared to your sugar formula.</p>
					</div>
				</div>
				<div class="col-sm-4 color-2">
					<div class="st-full-bg-col-in">
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;"><img src="../images/productos/icon-keep-taste.png" /></p>
						<h2 class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;">KEEP<br/>YOUR TASTE<br/>&nbsp;</h2>
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.3s" style="animation-delay: 0.3s;">Excellent flavor profiles for food and beverages.</p>
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.3s" style="animation-delay: 0.3s;">Excellent quality and composition of our stevia leaves.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row product-thumb" id="stevio">
			<div class="col-xs-12">
				<div class="row">
					<div class="col-xs-5 hidden-xs">
					</div>
					<div class="col-xs-12 col-sm-7">
						<h2>STEVI-O</h2>
						<p><strong>Stevia extract</strong></p>
						<p>Stevi-o is a natural non-caloric sweetener extracted from the stevia leaves of the plant. It does not contain high calories but a high sweetness, which is between 150 and 200 times sweeter than cane sugar.</p>
						<p><a href="stevi-o.php" class="button">Learn more</a></p>
						<i class="leaf hidden-xs hidden-sm hidden-md"></i>
					</div>
				</div>
			</div>
		</div>
		<div class="row product-thumb" id="stevix">
			<div class="col-xs-12">
				<div class="row">
					<div class="col-xs-12 col-sm-7">
						<h2>STEVI-X</h2>
						<p><strong>Solutions based on stevia extract</strong></p>
						<p>Stevi-x is our product of all-natural sweetener solutions based on our natural stevia extract stevia-o®.</p>
						<p>Each application is unique in its taste and sweetness. That's why we have our team of application technologists at your service to develop your specific natural sweetener solution, creating the perfect combination between your product and our natural solution.</p>
						<p><a href="stevi-x.php" class="button">Learn more</a></p>
						<i class="leaf hidden-xs hidden-sm hidden-md"></i>
					</div>
					<div class="col-xs-5 hidden-xs">
					</div>
				</div>
			</div>
		</div>
		<div class="st-mar-b-50 hidden-xs hidden-sm hidden-md"></div>
	</div>
</section>
<?php
include('include/footer.php');