<?php include('include/header.php'); ?>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area st-header-1" style="background-image:url('../images/trazabilidad-1.jpg')">
	<div class="container">
		<div class="st-tbl">
			<div class="st-tbl-cell">
				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Traceability</h1>
			</div>
		</div>
	</div>
</section>
<section class="st-middle-sec">
	<div class="st-common-sec st-twocol-desc-sec">
		<div class="container container-lg">
			<div class="st-biofabricaBx">
				<h2 class="st-color-secondary st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">100% Traceable</h2>
				<div class="row trazablesBx">
					<div class="col-md-6 st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">
						<h5 class="st-bot-margin-50">We identify, document and track each stage of the production process, from the seeds to the final production of stevia.</h5>
						<h6 class="st-color-primary">Traceability</h6>
						<p>In Stevia One, we know each plant of each batch, we know who worked on each plant, how many hours and days.</p>
					</div>					
					<div class="col-md-6 st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.4s">
						<p class="st-bot-margin-30">We register with accuracy in what date the batch was cropped, from which field, in which days it was sent to the production plant, as well as all the raw materials used.</p>
						<p>Thanks to traceability, all the historical data gets registered. And in case we need  it, we have the ability to identify the origin and evolution of each batch immediately.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	
    <div class="st-common-sec st-info-sec st-info-sec-2 st-trazabilidad-info-sec">
		<div class="container masqueBx">
			<h3 class="st-green st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s"><span class="disblk">Traceability is one of our competitive advantages.</span><br>We know and control each stage of the production process from beginning to end.</h3>
		</div>
	</div>
	




<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->


	<div class="st-newsletter-hidden"><?php include('include/newsletter.php') ?></div>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</section>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<?php include('include/footer.php'); ?>