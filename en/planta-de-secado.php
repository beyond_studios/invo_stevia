<?php include('include/header.php'); ?>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area st-header-1" style="background-image:url('../images/secado-1.jpg')">
    <div class="container">
        <div class="st-tbl">
            <div class="st-tbl-cell">
                <h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Drying Plant</h1>
            </div>
        </div>
    </div>
</section>
<section class="st-middle-sec">

    <div class="st-common-sec st-planta-secado-sec">
        <div class="container">
            <div class="row st-tbl-row st-tbl-md-row st-tbl-align-middle">
                <div class="col-md-6" id="mision">
                    <div class="composBx">
                        <h2>Drying Plant</h2>
                        <h6 class="st-grey">After their harvest, the leaves of stevia are subject to a drying process.</h6>
                        <ul class="st-bullet-list st-bullet-list-plain st-bullet-space">
                            <li>This process involves a cleaning and indirect drying.</li>
                            <li>The stevia leaves are never in contact with other chemical products.</li>
                            <li>Each production batch of dry leaves has complete traceability and is subject to strict quality control.</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6" id="vision">
                    <div class="secodoBx">
                        <h5 class="st-color-secondary">In Stevia One, we have strict quality controls in drying the leaves to ensure that the principal input of the production plant is in compliance with the required standards.</h5>
                    </div>
                </div>
            </div> 
            <div class="row st-tbl-row st-tbl-md-row st-tbl-align-middle">
                <div class="col-md-6">
                    <div class="composBx">
                        <h2>Packing</h2>
                        <ul class="st-bullet-list st-bullet-list-plain st-bullet-space">
                            <li>The stevia leaves are pressed and hermetically packed.</li>
                            <li>The packs are labelled to facilitate their identification as part of the traceability process.</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6">
                    
                </div>
            </div>          
        </div>
    </div>
    

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                    NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->


    <div class="st-newsletter-hidden"><?php include('include/newsletter.php') ?></div>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</section>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<?php include('include/footer.php'); ?>