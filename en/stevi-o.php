<?php
include('include/header.php');
?>
<section class="st-header-area st-header-1" style="background-image: url('../images/background-productos.png')">
	<div class="container">
		<div class="st-tbl">
			<div class="st-tbl-cell">
				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Productos</h1>
			</div>
		</div>
	</div>
</section>
<section class="st-middle-sec">
	<section class="container title">
		<div class="row">
			<div class="col-xs-12">
				<h2>STEVI-O</h2>
			</div>
		</div>
	</section>
	<section class="container body-content">
		<div class="row">
			<div class="col-xs-12">
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<h3>Stevia extract</h3>
						<p>Stevi-o is a natural non-caloric sweetener extracted from the stevia leaves. It does not contain high calories but a high sweetness, which is between 150 and 200 times sweeter than sugar cane.</p>
						<h3>Products Features:</h3>
						<ul>
							<li>High solubility in water and alcohol bases</li>
							<li>High stability in acid and alkaline solutions (pH 4-9)</li>
							<li>High tolerance to temperatures up to 196ºC</li>
							<li>High stability under light</li>
							<li>Does not affect the denture</li>
							<li>All natural, without chemical synthesis ingredients</li>
							<li>It does not ferment</li>
						</ul>
						<h3>Applications:</h3>
						<p>As a natural sweetener for food and beverages.</p>
					</div>
					<div class="col-xs-12 col-sm-6">
						<img src="../images/stevi-o/stevio-big.png" title="Stevi-O" class="hidden-xs" />
						<p class="buttons transparent"><a href="#" class="button-transparent">DOWNLOAD TECHNICAL SHEET</a></p>
						<i class="leaf hidden-xs hidden-sm hidden-md"></i>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p class="buttons"><a href="contact-us.php" class="button">CONTACT US</a></p>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>
<?php
include('include/footer.php');