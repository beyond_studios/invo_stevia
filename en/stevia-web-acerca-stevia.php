<?php include('include/header.php'); ?>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area st-header-1" style="background-image:url('../images/stevia-web-acerca-stevia-bg-1.jpg')">
    <div class="container">
        <div class="st-tbl">
            <div class="st-tbl-cell">
                <h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">About stevia</h1>
            </div>
        </div>
    </div>
</section>
<section class="st-middle-sec">

    <div class="st-common-sec st-twocol-desc-sec st-acerca-web-sec">
        <div class="container">
            <div class="row">
                <div class="col-sm-6" id="mision">                    
                    <h2 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">About stevia</h2>
                    <p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
                        <strong>The stevia is a native plant of South America.</strong> For centuries, the people from Brazil and Paraguay use the leaves to sweeten their food and beverages. <br><br><strong>The powder of the stevia leaves sweetens 30 times more than</strong> sugar. <strong>The extract of the stevia leaves</strong> can be <strong>400 times sweeter than</strong> sugar. And what is best is that pure stevia extract has <strong>zero calories!</strong>
                    </p>                    
                </div>
                <div class="col-sm-6" id="vision">                    
                    <h2 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Do you know that.. </h2>

                    <ul class="st-bullet-list st-bullet-list-plain st-bullet-list-primary st-bullet-space st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
                        <li>The stevia<strong> is photostable and thermostable</strong> at 95ºC.</li>
                        <li>The stevia<strong> is preserved in optimal conditions for 3 years</strong> approximately.</li>                            
                    </ul>                    
                </div>
            </div>          
        </div>
    </div>

    <div class="st-common-sec st-info-sec st-info-sec-1 st-info-acerca-web-sec">
        <div class="container">
            <h3 class="st-color-secondary st-underline-secondary st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s" >How good is the stevia!<br><span>And the secret ingredient is in its potential...</span></h3>
        </div>
    </div>

    

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                    NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->


    <div class="st-newsletter-hidden"><?php include('include/newsletter.php') ?></div>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</section>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<?php include('include/footer.php'); ?>