<?php include('include/header.php'); ?>
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area st-header-1" style="background-image: url('../images/banner_kurmi63-1.jpg')">
	<div class="container">
		<div class="st-tbl">
			<div class="st-tbl-cell">
				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">KURMI-63</h1>
			</div>
		</div>
	</div>
</section>
<section class="st-middle-sec">
	<div class="st-common-sec st-twocol-desc-sec st-portfolio-sg98-sec">
		<div class="container">
			<div class="row">
				<div class="col-md-6" id="mision">
					
						<h2 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">DESCRIPTION</h2>
						<p class="st-animate st-top-margin-25" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">SG98 Reb A 63 Steviol Glycosides is an all-natural & non-caloric sweetener extracted from the plant stevia leaves. It contains no calories but high sweetness, which is between 150 - 200 times sweeter than cane sugar.</p>
					
				</div>
				<div class="col-md-6" id="vision">					
					<h2 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">CHARACTERISTICS</h2>
					<ul class="st-bullet-list st-bullet-list-plain st-bullet-space st-animate st-top-margin-25" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
						<li>High solubility in water and alcohol bases.</li>
						<li>High stability in acid and alkaline solutions (pH4-9).</li>
						<li>High tolerance to temperatures of up to 196ºC.</li>
						<li>High stability under light.</li>
						<li>Teeth friendly.</li>
						<li>All natural, no chemical synthesis ingredients.</li>
						<li>Non-fermenting.</li>
					</ul>					
				</div>
			</div>
		</div>
	</div>
	<div class="st-common-sec st-image-sec st-image-sec-1" style="background-image: url('../images/applicationes-bg.jpg')">
		<div class="container container-xl">
			<h2 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Application</h2>
			<div class="row">
				<div class="col-sm-6">
					<ul class="st-bullet-list st-bullet-list-plain st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
						<li>As a natural sweetener for food & beverage products.</li>
					</ul>
				</div>
				<!--<div class="col-sm-6">
					<ul class="st-icon-list st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.5s">
						<li><span>Solicitar Muestra</span><i class="sicon-muestra"></i></li>
						<li><span>Descargar Ficha Técnica</span><i class="sicon-d-arrow-down"></i></li>
					</ul>
				</div>-->
			</div>
		</div>
	</div>
	<!--<div class="st-common-sec st-product-detail-big-btns-sec">
		<div class="container">
			<div class="row st-big-btn-row">
				<div class="col-xs-6 text-right">
					<a href="#" class="xtra-lg-btn st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s"><i class="sicon-arrow-s-left btnArrow"></i><span>ver nuestros<br>anterior
					</span></a>
				</div>
				<div class="col-xs-6">
					<a href="#" class="xtra-lg-btn st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.5s"><span>ver producto<br> siguiente
					</span><i class="sicon-arrow-s-right btnArrow"></i></a>
				</div>
			</div>
		</div>
	</div>-->
	<div class="container container-xl">
		<hr class="st-hr-space">
	</div>
	<div class="st-common-sec st-product-detail-form st-contact-form-sec st-contact-form-sec-new st-portfolio-sg98-contact">
		<div class="container">
			<h3 class="text-uppercase st-green st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">Contact us</h3>
			<?php include('include/contact.php')?>
		</div>
	</div>
	<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                    NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
    <div class="st-newsletter-hidden"><?php include('include/newsletter.php')?></div>
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
</section>
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<?php include('include/footer.php'); ?>