<?php include('include/header.php'); ?>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area st-header-1 st-header-beneficios" style="background-image:url('../images/beneficios-1.jpg')">
    <div class="container">
        <div class="st-tbl">
            <div class="st-tbl-cell">
                <h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Benefits</h1>
            </div>
        </div>
    </div>
</section>
<section class="st-middle-sec">

    <div class="st-common-sec st-info-sec st-big-desc st-beneficios-desc-sec">
        <div class="container">
            <h3 class="st-color-secondary st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s" >Gain health and well-being!</h3>
            <p class="st-font-light st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.4s"><strong>Do you know that...</strong></p>
            <p>The glycoside of steviol –the extract of the stevia plant – have zero calories, do not affect blood pressure, blood glucose levels,or put dental health at  risk.</p>
            <p class="st-font-light st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.4s"><strong>Can you imagine</strong></p>
            <p>A life with products based on stevia?<br>
            In <a href="javascript:void(0)" class="st-link-secondary">Stevia One,</a> Yes!</p>
        </div>
    </div>

    <div class="st-common-sec st-full-bg-columns st-beneficios-img-sec">
        <div class="container-fluid">
            <div class="row st-tbl-row st-tbl-md-row">
                <div class="col-md-6" id="mision" style="background-image:url('../images/la-diabetes-dejo.jpg')">
                    
                </div>
                <div class="col-md-6 st-bg-theme" id="vision">
                    <div class="st-full-bg-col-in">
                        <h3 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">The diabetes is not bitter anymore</h3>
                        <p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">According to the criteria of The Joint FAO/WHO Expert Committee on Food Additives (JECFA), the clinical studies demonstrate that <strong>the glycosides of steviol do not affect the blood pressure or the blood glucose levels.</strong></p>
                        <p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s"><strong>Can you imagine?</strong></p>
                        <p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">The diabetics and the children enjoying the benefits of stevia?</p>
                        <p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s"><strong>In Stevia One, Yes!</strong><br>Diabetics enjoy the sweetness of stevia!
                         </p>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <div class="st-common-sec st-big-desc st-big-desc-1">
        <div class="container">            
            <p class="st-font-light st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.4s"><strong>Smile without cavities</strong></p>
            <p class="st-font-light st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.4s">Do you know that...<br>
The bacteria that causes cavity can consume stevia, but cannot digest it and as a consequence it dies. Then, a battle between the stevia vs. the cavities, the stevia wins.</p>
        </div>
    </div>

    

    

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                    NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->


    <div class="st-newsletter-hidden"><?php include('include/newsletter.php') ?></div>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</section>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<?php include('include/footer.php'); ?>