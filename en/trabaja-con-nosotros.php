<?php include('include/header.php'); ?>
<section class="st-header-area st-header-1" style="background-image: url('../images/background-unique-proposal.png')">
	<div class="container">
		<div class="st-tbl">
			<div class="st-tbl-cell">
				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Work With Us</h1>
			</div>
		</div>
	</div>
</section>
<section class="st-middle-sec">
	<div class="st-common-sec st-contact-trabaja-desc">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h2>WE ARE STEVIA ONE, AN AGROINDUSTRIAL COMPANY<br/>PERUVIAN, FOUNDED IN 2009.</h2>
					<p>We have operations in the Central Jungle and the North Coast of the Country. We are dedicated to the planting, cultivation, harvest and extraction of Stevia, naturally.</p>
					<p>Our singular process is respectful with the Environment, and it handles high standards of Quality and Efficiency</p>
					<p>Our product ensures its traceability and good taste.</p>
					<p>Our people, is consistent with our philosophy. People "Passionate about Health".</p>
					<p>With Stevia One you will have the opportunity to participate in a company that seeks to contribute to make our world healthier.</p>
					<p>We are oriented to achieve growth, of all the interested parties. Our collaborators, suppliers, customers and shareholders</p>
					<p class="boxed">If you are ready to be part of our team, send us your CV along with a motivation letter to<br/><a href="mailto:seleccion@steviaone.com">seleccion@steviaone.com</a></p>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
include('include/footer.php');
