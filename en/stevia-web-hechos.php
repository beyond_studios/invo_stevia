<?php include('include/header.php'); ?>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area st-header-1" style="background-image:url('../images/stevia-web-hechos-bg-1.jpg')">
    <div class="container">
        <div class="st-tbl">
            <div class="st-tbl-cell">
                <h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Present</h1>
            </div>
        </div>
    </div>
</section>
<section class="st-middle-sec">

    <div class="st-common-sec st-twocol-desc-sec st-actualidad-sec">
        <div class="container">
            <div class="row">
                <div class="col-sm-6" id="mision">
                    <div class="st-full-bg-col-in">
                        <h2 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Obesity! A disease of weight</h2>                        
                        <ul class="st-bullet-list st-bullet-list-plain st-bullet-space st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.5s">
                            <li><strong>Facts</strong></li>
                            <li>Millions of people around the world are obese.</li>
                            <li>2012, WHO estimated that more than 40 million of children under 5 years already are overweight or are obese.</li>
                            <li><strong>2030, the obesity will be one of the principal causes of death.</strong></li>
                            <li>Sedentary life, bad eating habits or the mixture of both are the main causes of obesity.</li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6" id="vision">
                    <div class="st-full-bg-col-in">
                        <h2 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Cavities, enemy of the mouth health</h2>                        
                        <ul class="st-bullet-list st-bullet-list-plain  st-bullet-space st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.5s">
                            <li><strong>Facts</strong></li>
                            <li><strong>Around the world, 90% of children in school age and 100% of the adults have cavities.</strong> </li>
                            <li>30% of the adults between 65 and 74 years have no natural teeth.</li>
                            <li><span class="st-color-primary">The cavities are the number one enemy of mouth health.<br><strong>Today, sugar has a bitter taste!</strong></span></li>
                        </ul>                        
                    </div>
                </div>
            </div>   
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="st-twocol-extra-col">
                        <h2 class="st-color-secondary st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">The diabetes, the bad sweet</h2>
                        <p class="st-font-light st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">The diabetes is a chronic disease that raises blood glucose levels, being able to damage the eyes and kidney.<br><br>And where is the glucose? In food.</p>
                        <p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.3s"><strong>Facts</strong></p>
                        <ul class="st-bullet-list st-bullet-list-plain st-bullet-space st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.7s">
                            <li class="st-font-light">2013, WHO estimated that 347 million of people in the world already have diabetes.</li>
                            <li><strong>Towards 2035, the International Diabetes Federation (IDF), estimated that 592 million of people will have diabetes. This will cost the health system around US$ 500 billion dollars, more than 10% of the health budget worldwide.</strong></li>

                        </ul>
                    </div>
                </div>
            </div>       
        </div>
    </div>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                    NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->


    <div class="st-newsletter-hidden"><?php include('include/newsletter.php') ?></div>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</section>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<?php include('include/footer.php'); ?>