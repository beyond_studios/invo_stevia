<?php include('include/header.php'); ?>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area st-header-1" style="background-image:url('../images/contact-bg-2-1.jpg')">
	<div class="container">
		<div class="st-tbl">
			<div class="st-tbl-cell">
				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Research and<br> Development (R&amp;D)</h1>
			</div>
		</div>
	</div>
</section>
<section class="st-middle-sec">
	<div class="st-common-sec st-twocol-desc-sec">
		<div class="container">
			<div class="st-biofabricaBx">
				<h2>Research and<br> Development (R&amp;D)</h2>
				<div class="row">
					<div class="col-md-6">
						<ul class="st-bullet-list st-bullet-list-plain st-bullet-space">
							<li>It goes across all process stages.</li>							
							<li>We have our own HPLC equipment to control the quality of each production batch.</li>
						</ul>
					</div>					
					<div class="col-md-6">						
						<ul class="st-bullet-list st-bullet-list-plain st-bullet-space">
							<li>The genetic material is the result of an investigation and continuous genetic improvement.</li>
							<li>The performance and genetic quality of the plants of Stevia One are within the highest standards of the world.</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	
    <div class="st-common-sec st-info-sec st-testimonials-sec">
        <div class="container masqueBx">
            
                
                <blockquote>
                    <p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s" >“Stevia One is my job, and is also the project where I feel I’m part of a macro change every day. Yes, that’s what I like since the beginning. And that’s what workers feel from farmer to directors.</p><p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">Stevia One is an example that companies can do good things, they can earn and at the same time they are creating benefit for people and the planet”.</p>
                    <div class="st-author-info st-author-info-1 st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.2s" style="animation-delay: 0.2s;">
                        <h5>Alejandro “Jano” Segovia</h5>
                        <span class="st-author-post">R&amp;D Manager</span>
                        <span class="st-author-company">Agronomist Engineer</span>
                    </div>
                </blockquote>
                
            
        </div>
    </div>
	




<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->


	<div class="st-newsletter-hidden"><?php include('include/newsletter.php') ?></div>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</section>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<?php include('include/footer.php'); ?>