<?php
include('include/header.php');
?>
<section class="st-header-area st-header-1" style="background-image: url('../images/somos-bg.jpg')">
	<div class="container">
		<div class="st-tbl">
			<div class="st-tbl-cell">
				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">ABOUT US</h1>
			</div>
		</div>
	</div>
</section>
<section class="st-middle-sec">
	<div class="st-common-sec st-common-center-text">
		<div class="container">
			<p>At Stevia One we are dedicated since 2009 to research stevia. In addition, we want to contribute
to the sustainable growth of the planet and care of the environment, planting, cultivating,
harvesting and producing a stevia with a singular process of extraction and purification.</p>
			<p class="text-green">WITHOUT CHEMICALS! WITHOUT ALCOHOL!</p>
		</div>
	</div>
	<div class="st-common-sec st-full-bg-columns st-full-bg-columns-1">
		<div class="container">
			<div class="row st-tbl-row">
				<div class="col-sm-6">
					<div class="st-full-bg-col-in" id="misionEN">
						<h2 class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;">MISSION</h2>
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.3s" style="animation-delay: 0.3s;"><strong>We are Passionate about Health:</strong> Globalizing the availability of our natural stevia extract for consumption, we help to live in a healthier world, fighting diabetes and obesity.</p>
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.3s" style="animation-delay: 0.3s;"><strong>We are respectful with our Stakeholders:</strong> They are all part of our success.</p>
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.3s" style="animation-delay: 0.3s;"><strong>We are Conscientious of the Environment:</strong> We think and act in a sustainable manner throughout all our production processes.</p>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="st-full-bg-col-in" id="visionEN">
						<h2 class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;">VISION</h2>
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.3s" style="animation-delay: 0.3s;">To be the most important global supplier of stevia produced naturally.</p>
					</div>
					<div class="st-full-bg-col-in" id="valoresEN">
						<h2 class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;">VALUES</h2>
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.3s" style="animation-delay: 0.3s;">We are <strong>responsible</strong><br/>
						We are <strong>efficient</strong><br/>
						We are <strong>respectful</strong><br/>
						We are <strong>consistent</strong></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
include('include/footer.php');