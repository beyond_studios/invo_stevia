<?php
include('include/header.php');
?>
<section class="st-header-area st-header-1" style="background-image: url('../images/rainforest_1-1.jpg')">
	<div class="container">
		<div class="st-tbl">
			<div class="st-tbl-cell">
				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Our One Propostal</h1>
			</div>
		</div>
	</div>
</section>
<section class="st-middle-sec">
	<div class="st-common-sec st-full-bg-columns st-full-bg-columns-1">
		<div class="container">
			<div class="row st-tbl-row">
				<div class="col-sm-4 color-1">
					<div class="st-full-bg-col-in">
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;"><img src="../images/nuestra-propuesta-unica/icon-natural-process.png" /></hp>
						<h2 class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;">PURE AND NATURAL<br/>PROCESS</h2>
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.3s" style="animation-delay: 0.3s;">A singular water-based stevia extraction process that results in our incredible natural stevia extract.</p>
					</div>
				</div>
				<div class="col-sm-4 color-2">
					<div class="st-full-bg-col-in">
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;"><img src="../images/nuestra-propuesta-unica/icon-traceable.png" /></hp>
						<h2 class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;">FROM THE SEED<br/>UNTIL THE FINAL PRODUCT</h2>
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.3s" style="animation-delay: 0.3s;">Reliability through vertical integration and constant focus on quality.</p>
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.3s" style="animation-delay: 0.3s;">From the seeds to the final product, we can track and control every batch of stevia we produce.</p>
					</div>
				</div>
				<div class="col-sm-4 color-3">
					<div class="st-full-bg-col-in">
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;"><img src="../images/nuestra-propuesta-unica/icon-rainforest.png" /></hp>
						<h2 class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;">RAINFOREST<br/>ALLIANCE</h2>
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.3s" style="animation-delay: 0.3s;">Our stevia products proudly carry the Rainforest Alliance Certified seal, which complies with the sustainability environmental, social and economic comprehensive standards.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
include('include/footer.php');