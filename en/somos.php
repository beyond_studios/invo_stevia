<?php include('include/header.php'); ?>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area st-header-1" style="background-image:url('../images/somos-bg.jpg')">
	<div class="container">
		<div class="st-tbl">
			<div class="st-tbl-cell">
				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">WE ARE STEVIA ONE</h1>
			</div>
		</div>
	</div>
</section>
<section class="st-middle-sec">
	<div class="st-common-sec st-somos-video-sec">
		<div class="container container-md st-md-content">
			<div class="st-vimeo-video-bx st-vimeo-video-bx-wide">
			   <div class="embed-responsive embed-responsive-16by9">
				<iframe src="https://player.vimeo.com/video/189229182" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>
			</div>
		</div>
	</div>
	<div class="st-common-sec st-theme-grey-sec">
		<div class="container container-md st-md-content">
			<div class="st-somosBx">
				<p>In Stevia One we have dedicated five years to stevia research. More importantly, we want to contribute to the sustainable growth of the planet, take care of the environment, produce stevia with a unique extraction and purification process.</p>
				<h3 class="st-grey st-animate animated fadeInUp">Without chemicals! Without alcohol!</h3>
			</div>
		</div>
	</div>
	<div class="st-common-sec st-info-sec st-testimonials-sec">
		<div class="container">
			<div class="row">
				<div class="col-md-1"></div>
				<blockquote class="col-md-10">
					<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;">“I feel proud to work at Stevia One because the people is participative and cooperative. We all support each other. We are a team. Also, I appreciate every opportunity because they train us. Our job is going to be good for all, and not only in Peru, but also in the world”.</p>
					<div class="st-author-info st-author-info-1 st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.2s" style="animation-delay: 0.2s;">
						<h5>Erlinda Calle Rojas </h5>
						<span class="st-author-post">Human Resources Assistant</span>
					</div>
				</blockquote>
				<div class="col-md-1"></div>
			</div>
		</div>
	</div>
	




<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                    NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
    <div class="st-newsletter-hidden"><?php include('include/newsletter.php');?></div>
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</section>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<?php include('include/footer.php'); ?>