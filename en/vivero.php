<?php include('include/header.php'); ?>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area st-header-1" style="background-image:url('../images/nuestra-bg-1.jpg')">
	<div class="container">
		<div class="st-tbl">
			<div class="st-tbl-cell">
				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Nursery</h1>
			</div>
		</div>
	</div>
</section>
<section class="st-middle-sec">
	<div class="st-common-sec st-vivero-sec">
		<div class="container">
			<div class="st-vivero-desc">
				<h2 class="st-bot-margin-40">Nursery</h2>
				<ul class="st-bullet-list st-bullet-list-plain st-bullet-space">
					<li>Is the place for acclimation of the plants that come from the bioplant.</li>
					<li>Has the function to spread the plants that comes from the cuttings.</li>
					<li>Is the place where the production of seeding is performed.</li>
				</ul>
			</div>
		</div>
	</div>
	
	<div class="st-common-sec st-info-sec st-testimonials-sec st-info-vivero-sec">
		<div class="container">
			<blockquote>
				<p class="st-animate st-bot-margin-30" data-os-animation="fadeInUp" data-os-animation-delay="0s">“Stevia One is my second family. I have almost 6 years in the Company. Is my first job, and more than a job coming here makes me feel complete.</p>
				<p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">Our security policies, care of the environment, waste management and efficient use of water has become the values that I share with my family. And I’m sure that I’m not the only one, this is extended to all the Stevia One family”.</p>
				<div class="st-author-info st-author-info-1 st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">
					<h5>David Paredes</h5>
					<span class="st-author-post">Nursery Chief</span>
					<span class="st-author-company">Agronomist Engineer</span>
				</div>
			</blockquote>
		</div>
	</div>




<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->


	<div class="st-newsletter-hidden"><?php include('include/newsletter.php') ?></div>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</section>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<?php include('include/footer.php'); ?>