<?php include('include/header.php'); ?>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area st-header-1" style="background-image:url('../images/integracion-bg-1.jpg')">
	<div class="container">
		<div class="st-tbl">
			<div class="st-tbl-cell">
				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Vertical Integration</h1>
			</div>
		</div>
	</div>
</section>
<section class="st-middle-sec">
	<div class="st-common-sec st-integracion-vertical-sec">
		<div class="container">
			<div class="st-biofabricaBx">
				<h2 class="st-color-secondary">Vertical Integration</h2>
				<div class="row">
					<div class="col-md-6">
						<h3 class="st-bot-margin-30">The Vertical Integration is another of the competitive advantages of Stevia One because it facilitates:</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">						
						<ol class="st-ol-listing st-ol-listing-plain st-bullet-space" start="0">
							<li>The control of all the processes.</li>
							<li>The efficiency and synchronization of the supply chain.</li>
							<li>The continuous improvement. Continuous development of new diversities of high quality starting from our plants.</li>
						</ol>
					</div>
					<div class="col-md-6">
						<ol class="st-ol-listing st-ol-listing-plain st-bullet-space" start="3">							
							<li>The quality assurance of all the productive process, from seeds to final product.</li>
							<li>The guarantee of the process is socio-environment sustainable.</li>
							<li>Trust and safety.</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="st-common-sec st-info-sec st-info-sec-1 st-integrecion-info-sec">
		<div class="container masqueBx">
			<h3 class="st-green st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.2s"><span class="disblk">In terms of efficiency and sustainability, the Vertical Integration allows for an optimization of resources.</span><br>Stevia One,<br>We have a commitment with every customer.</h3>
		</div>
	</div>
	




<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

	<div class="st-newsletter-hidden"><?php include('include/newsletter.php') ?></div>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</section>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<?php include('include/footer.php'); ?>