<?php include('include/header.php'); ?>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
					MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area st-header-2" style="background-image:url('../images/trazabilidad-head.jpg')">
	<div class="container">
		<div class="st-tbl">
			<div class="st-tbl-cell">
				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0">100%<br>
				Traceable <small>We are vertically integrated</small></h1>

			</div>
		</div>
		<a href="#st-middle-content" class="st-header-link st-smooth-scroll st-animate" data-os-animation="fadeInDown" data-os-animation-delay="0"><i class="sicon-arrow-l-down"></i></a>
	</div>
</section>
<section class="st-middle-sec" id="st-middle-content">
	
	<div class="st-common-sec st-round-icon-sec st-trazables-sec">
		<div class="container">
			<div class="row">
			  <div class="st-tbl-row st-rounded-icon-row">
				<div class="col-sm-4">

					<div class="st-main-icon-l text-right st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0">

						<i class="sicon-estamos"></i>

					</div>

				</div>
				<div class="col-sm-8">
					<h2 class="st-small-desc st-trazables-heading st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">We are vertically integrated. We document and track every batch from the seeds to the final product.</h2>

				</div>
			</div>
			</div>
		</div>
	</div>
	
	<div class="st-common-sec st-info-sec st-info-icon-sec">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<i class="sicon-trazables"></i>
					<p class="st-info-block st-animate st-font-quicksand"  data-os-animation="fadeInUp" data-os-animation-delay="0s">
						We really are <br>100% traceable
					</p>
				</div>
				<div class="col-sm-4">
					<i class="sicon-shipping"></i>
					<p class="st-info-block st-animate st-font-quicksand"  data-os-animation="fadeInUp" data-os-animation-delay="0.4s">
						From nursery<br>
						to final product
					</p>
				</div>
				<div class="col-sm-4">
					<i class="sicon-field"></i>
					<p class="st-info-block st-animate st-font-quicksand"  data-os-animation="fadeInUp" data-os-animation-delay="0.8s">
						We have<br>
						our own fields
					</p>
				</div>
			</div>
		</div>
	</div>
	

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                    NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
    <div class="st-newsletter-hidden"><?php include('include/newsletter.php');?></div>
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</section>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<?php include('include/footer.php'); ?>