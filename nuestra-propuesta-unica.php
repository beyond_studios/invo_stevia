<?php
include('include/header.php');
?>
<section class="st-header-area st-header-1" style="background-image: url('images/rainforest_1-1.jpg')">
	<div class="container">
		<div class="st-tbl">
			<div class="st-tbl-cell">
				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Nuestra propuesta única</h1>
			</div>
		</div>
	</div>
</section>
<section class="st-middle-sec">
	<div class="st-common-sec st-full-bg-columns st-full-bg-columns-1">
		<div class="container">
			<div class="row st-tbl-row">
				<div class="col-sm-4 color-1">
					<div class="st-full-bg-col-in">
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;"><img src="images/nuestra-propuesta-unica/icon-natural-process.png" /></hp>
						<h2 class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;">PROCESO<br/>PURO Y NATURAL</h2>
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.3s" style="animation-delay: 0.3s;">Un proceso único de extracción de stevia a base de agua, que da como resultado nuestro increíble extracto de stevia natural.</p>
					</div>
				</div>
				<div class="col-sm-4 color-2">
					<div class="st-full-bg-col-in">
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;"><img src="images/nuestra-propuesta-unica/icon-traceable.png" /></hp>
						<h2 class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;">DESDE LA SEMILLA<br/>HASTA EL PRODUCTO FINAL</h2>
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.3s" style="animation-delay: 0.3s;">Confiabilidad a través de la integración vertical y enfoque constante en la calidad.</p>
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.3s" style="animation-delay: 0.3s;">Desde las semillas hasta el producto final, podemos rastrear y controlar cada lote de stevia que producimos.</p>
					</div>
				</div>
				<div class="col-sm-4 color-3">
					<div class="st-full-bg-col-in">
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;"><img src="images/nuestra-propuesta-unica/icon-rainforest.png" /></hp>
						<h2 class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;">RAINFOREST<br/>ALLIANCE</h2>
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.3s" style="animation-delay: 0.3s;">Nuestros productos stevia llevan con orgullo el sello Rainforrest Alliance Certified, que cumple con los estándares integrales de sostenibilidad ambiental, social y económica.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
include('include/footer.php');