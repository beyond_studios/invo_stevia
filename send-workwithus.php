<?php
error_reporting(E_ALL);
include ('PHPMailer-master/PHPMailerAutoload.php');

$response=new stdClass();
$response->isError=FALSE;
$response->message='El mensaje se ha enviado con éxito';
try
{
	$oficina=isset($_POST['dropDownListOficina'])?trim($_POST['dropDownListOficina']):"";
	$telefono=isset($_POST['textBoxTelefono'])?trim($_POST['textBoxTelefono']):"";
	$nombre=isset($_POST['textBoxNombre'])?trim($_POST['textBoxNombre']):"";
	$apellidos=isset($_POST['textBoxApellidos'])?trim($_POST['textBoxApellidos']):"";
	$pais=isset($_POST['dropDownListPais'])?trim($_POST['dropDownListPais']):"";
	$email=isset($_POST['textBoxEmail'])?trim($_POST['textBoxEmail']):"";
	if (trim($email)=='')
		throw new Exception("El campo e-mail es obligatorio.");
	$html="";
	$html.="Se ha recibido un mensaje de trabaja con nosotros. A continuación los datos:<br />";
	$html.="<br />";
	$html.="Oficina: ".$oficina."<br />";
	$html.="Teléfono: ".$telefono."<br />";
	$html.="Nombre: ".$nombre."<br />";
	$html.="Apellidos: ".$apellidos."<br />";
	$html.="E-Mail: ".$email."<br />";
	$mail=new PHPMailer(true);
	try
	{
		$mail->From=$email;
		$mail->FromName=$nombre;
		$mail->setFrom($email, $nombre);
		// $mail->addAddress('luis@involucra.com', 'Luis Guillén');
		$toEmail='seleccion@steviaone.com';
		$mail->addAddress($toEmail, 'Stevia One');
		$mail->addAddress('eduardo@involucra.com', 'Eduardo Páez Trujillo');
		$mail->Subject='Mensaje de trabaja con nosotros del sitio Stevia One';
		$mail->IsHTML(true);
		$mail->CharSet='UTF-8';
		$mail->Body=$html;
		foreach ( $_FILES as $file )
		{
			if ($file['tmp_name']=='')
				continue;
			$mail->AddAttachment($file['tmp_name'], $file['name']);
		}
		$mail->send();
	}
	catch ( phpmailerException $e )
	{
		throw new Exception($e->errorMessage());
	}
	catch ( Exception $e )
	{
		throw new Exception($e->getMessage());
	}
}
catch ( Exception $ex )
{
	$response->isError=TRUE;
	$response->message=$ex->getMessage();
}
echo json_encode($response);