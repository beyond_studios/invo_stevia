var isMobile = false;
var md = new MobileDetect(window.navigator.userAgent);
isMobile = (md.phone()!=null);

(function($)
{
	if($('#bg-video').length)
	{
		videojs('bg-video').Background(
		{
			container: 'st-discover-video',
			autoPlay: true
		});
	}
	$('.navbar-toggle').on('click', function()
	{
		if($('.st-dropmenu').hasClass('st-menu-active'))
		{
			$('.st-dropmenu').removeClass('st-menu-active');
			/*
			 * $('.st-dropmenu').fadeOut(100, function(){
			 * $(this).removeClass('st-menu-active') });
			 */
			$('body').removeClass('st-menu-active');
			$('.navbar-toggle').addClass('collapsed');
		}
		else
		{
			$('.st-dropmenu').addClass('st-menu-active');
			/*
			 * $('.st-dropmenu').fadeIn(100, function(){
			 * $(this).addClass('st-menu-active') });
			 */
			$('body').addClass('st-menu-active');
			$('.navbar-toggle').removeClass('collapsed');
			submenuTitleSameHeight();
		}
	});
	$('.st-dropmenu .nav-pills a').on('click', function()
	{
		$('.navbar-toggle').click();
	})
	$('body').bind('click', function(e)
	{
		if($(e.target).closest('.st-dropmenu').length==0&&$(e.target).closest('.navbar-toggle').length==0)
		{
			// click happened outside of .navbar, so hide
			var opened=$('.st-dropmenu').hasClass('st-menu-active');
			if(opened===true)
			{
				// $('.navbar-collapse').collapse('hide');
				$('.navbar-toggle').click();
			}
		}
	});
	$('.st-dropmenu h4').each(function(index, element)
	{
		if($('ul', $(element).parent()).length>0)
			$(element).append('<a href="#" class="collapsed st-submenu-toggle"><i class="glyphicon glyphicon-chevron-up visible-xs-inline-block visible-sm-inline-block"></i></a>');
	})
	$('.st-dropmenu h4 .st-submenu-toggle, .st-dropmenu h4 a').on('click', function()
	{
		if($(window).width()<1024)
		{
			$('.st-dropmenu .collapse').collapse('hide');
			$(this).closest('h4').next().collapse('toggle');
		}
	});
	if($(window).width()<992)
	{
		$('.st-dropmenu .nav-pills').collapse('hide');
	}
	$('.st-dropmenu .nav-pills').on('hide.bs.collapse', function()
	{
		$(this).prev().find('.st-submenu-toggle').addClass('collapsed');
		$(this).prev().removeClass('st-menu-title-active');
	})
	$('.st-dropmenu .nav-pills').on('show.bs.collapse', function()
	{
		$(this).prev().find('.st-submenu-toggle').removeClass('collapsed');
		$(this).prev().addClass('st-menu-title-active');
	})
	function onScrollInit(items, trigger)
	{
		items.each(function()
		{	
			var osElement=$(this), osAnimationClass=osElement.attr('data-os-animation'), osAnimationDelay=osElement.attr('data-os-animation-delay');
			osElement.css(
			{
				'-webkit-animation-delay': osAnimationDelay,
				'-moz-animation-delay': osAnimationDelay,
				'animation-delay': osAnimationDelay
			});
			var osTrigger=(trigger)?trigger:osElement;
			osTrigger.waypoint(function()
			{
				osElement.addClass('animated').addClass(osAnimationClass);
			},
			{
				triggerOnce: true,
				offset: '90%'
			});
		});
	}
	if($("select").length)
	{
		$('select').selectpicker(
		{
			container: 'body'
		});
	}
	$('.st-input-file input').change(function()
	{
		$this=$(this);
		$default=$(this).closest('.st-input-file').data('placeholder');
		fileName=$($this).val().replace(/.*(\/|\\)/, '');
		if(fileName=='')
		{
			$(this).closest('label').find('span').text($default);
		}
		else
		{
			$(this).closest('label').find('span').text(fileName);
		}
	})
	$('.st-smooth-scroll').bind('click', function(event)
	{
		var $anchor=$(this);
		$('html, body').stop().animate(
		{
			scrollTop: $($anchor.attr('href')).offset().top-110
		}, 1800);
		event.preventDefault();
	});
	if($('body').hasClass('st-page-index')||$('body').hasClass('st-page-index-demo'))
	{
		$(window).load(function()
		{
			$('.st-page-preloader').fadeOut('slow');
			$(window).trigger('resize');
			scrollifySetup();
			onScrollInit($('.st-animate'));
		})
	}
	else
	{
		onScrollInit($('.st-animate'));
	}
	function submenuTitleSameHeight()
	{
		titleHeight=0;
		$('.st-dropmenu h4').each(function()
		{
			if($(this).find('span').outerHeight()>titleHeight)
			{
				titleHeight=$(this).find('span').outerHeight();
				console.log($(this).find('span').outerHeight());
			}
		});
		$('.st-dropmenu h4').css('min-height', titleHeight+1);
		// $('.st-dropmenu').css('height', $(window).height());
	}
	function scrollifySetup()
	{
		if($(window).height()>800)
		{
			window.scrollTo(0,0);
			setTimeout(function()
			{
				$.scrollify.destroy();
			}, 500);
			return;
		}
		$.scrollify.update();
		if(!isMobile && $(window).height()<800 && $('body').hasClass('st-page-index'))
		{
			$.scrollify(
			{
				section: ".st-scroll-section",
				sectionName: false,
				interstitialSection: ".navbar-default, .st-footer",
				standardScrollElements: ".st-dropmenu",
				easing: "easeOutExpo",
				scrollSpeed: 1100,
				before: function(index, sections)
				{
					console.log(index);
					console.log(sections);
					if(index==2)
					{
						$('body').addClass('st-home-video-animate');
					}
					else
					{
						$('body').removeClass('st-home-video-animate');
					}
				}
			});
		}
	}
	submenuTitleSameHeight();
	if(window.location.hash.length)
	{
		$('html,body').animate(
		{
			scrollTop: jQuery(window.location.hash).offset().top-100
		}, 900);
	}
	$(window).resize(function()
	{
		submenuTitleSameHeight();
		scrollifySetup();
		if($(window).width()>991)
		{
			$('.st-dropmenu .nav-pills').collapse('show');
		}
		else
		{
			// $('.st-dropmenu .nav-pills').collapse('hide');
		}
	})
	$(window).scroll(function()
	{
		if($(".navbar").offset().top>25)
		{
			$(".navbar-fixed-top").addClass("st-nav-collapsed");
		}
		else
		{
			$(".navbar-fixed-top").removeClass("st-nav-collapsed");
		}
	});
	$('.scrolling-button .icon-scroll').on('click', function()
	{
		if($('.scrolling-button').css('opacity')!=0)
			$.scrollify.move(2);
	});
	$('.scrolling-button').focus();


	/*add parallax*/
	var scene = document.getElementById('scene');
	var parallax = new Parallax(scene);

	/*preloader*/
	var cnt=document.getElementById("count"); 
	var water=document.getElementById("water");
	var percent=cnt.innerText;
	var interval;
	interval=setInterval(function(){ 
	  percent++; 
	  cnt.innerHTML = percent; 
	  water.style.transform='translate(0'+','+(100-percent)+'%)';
	  if(percent==100){
	    clearInterval(interval);
	    $('body').removeClass('loading').addClass('loaded');
	    setTimeout(function(){
	    	$('.preloader').hide();
	    }, 500);
	  }
	},75);


})(jQuery)