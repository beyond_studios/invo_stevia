<?php
include('include/header.php');
?>
<section class="st-header-area st-header-1" style="background-image: url('images/background-productos.png')">
	<div class="container">
		<div class="st-tbl">
			<div class="st-tbl-cell">
				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Productos</h1>
			</div>
		</div>
	</div>
</section>
<section class="st-middle-sec">
	<div class="st-common-sec st-common-center-text">
		<div class="container">
			<p class="text-green">REDUCCIÓN  DE CALORÍAS<br/>COMPLETAMENTE NATURAL</p>
		</div>
	</div>
	<div class="st-common-sec st-full-bg-columns st-full-bg-columns-1">
		<div class="container">
			<div class="row st-tbl-row">
				<div class="col-sm-4 color-1">
					<div class="st-full-bg-col-in">
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;"><img src="images/productos/icon-calorie-reduction.png" /></p>
						<h2 class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;">INTENSA<br/>REDUCCIÓN<br/>DE CALORÍAS</h2>
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.3s" style="animation-delay: 0.3s;">Expertos en la aplicación de stevia.</p>
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.3s" style="animation-delay: 0.3s;">Soluciones edulcorantes naturales para lograr las reducciones de calorías deseadas.</p>
					</div>
				</div>
				<div class="col-sm-4 color-4">
					<div class="st-full-bg-col-in">
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;"><img src="images/productos/icon-cost-effective.png" /></p>
						<h2 class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;">COSTO<br/>COMPETITIVO<br/>&nbsp;</h2>
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.3s" style="animation-delay: 0.3s;">Desarrollo de productos a medida para sus aplicaciones.</p>
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.3s" style="animation-delay: 0.3s;">La solución natural adecuada para usted, generando ahorros considerables en comparación con su fórmula de azúcar.</p>
					</div>
				</div>
				<div class="col-sm-4 color-2">
					<div class="st-full-bg-col-in">
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;"><img src="images/productos/icon-keep-taste.png" /></p>
						<h2 class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;">MANTIENE<br/>EL SABOR<br/>&nbsp;</h2>
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.3s" style="animation-delay: 0.3s;">Excelentes perfiles de sabor para Alimentación y bebidas.</p>
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.3s" style="animation-delay: 0.3s;">Excelente calidad y composición de nuestras hojas de stevia.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row product-thumb" id="stevio">
			<div class="col-xs-12">
				<div class="row">
					<div class="col-xs-5 hidden-xs">
					</div>
					<div class="col-xs-12 col-sm-7">
						<h2>STEVI-O</h2>
						<p><strong>Extracto de stevia</strong></p>
						<p>Stevi-o es un edulcorante natural no calórico extraído de las hojas de stevia de la planta. No contiene altas calorías sino un alto dulzor, que es entre 150 y 200 veces más dulce que el azúcar de caña.</p>
						<p><a href="stevi-o.php" class="button">DESCUBRE MÁS</a></p>
						<i class="leaf hidden-xs hidden-sm hidden-md"></i>
					</div>
				</div>
			</div>
		</div>
		<div class="row product-thumb" id="stevix">
			<div class="col-xs-12">
				<div class="row">
					<div class="col-xs-12 col-sm-7">
						<h2>STEVI-X</h2>
						<p><strong>Soluciones a base de extracto de stevia</strong></p>
						<p>Stevi-x es nuestro producto de soluciones edulcorantes totalmente naturales basado en nuestro extracto de stevia natural stevi-o®.</p>
						<p>Cada aplicación es única en su sabor y dulzura. Es por eso que tenemos nuestro equipo de tecnólogos de aplicaciones a su servicio para desarrollar su solución específica de edulcorante natural, creando la combinación perfecta entre su producto y nuestra solución natural.</p>
						<p><a href="stevi-x.php" class="button">DESCUBRE MÁS</a></p>
						<i class="leaf hidden-xs hidden-sm hidden-md"></i>
					</div>
					<div class="col-xs-5 hidden-xs">
					</div>
				</div>
			</div>
		</div>
		<div class="st-mar-b-50 hidden-xs hidden-sm hidden-md"></div>
	</div>
</section>
<?php
include('include/footer.php');