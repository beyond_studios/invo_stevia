<?php include('include/header.php'); ?>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area st-header-1" style="background-image:url('images/somos-stevia-one-bg-1.jpg')">
    <div class="container">
        <div class="st-tbl">
            <div class="st-tbl-cell">
                <h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Somos Stevia One</h1>
            </div>
        </div>
    </div>
</section>
<section class="st-middle-sec">

    <div class="st-common-sec st-full-bg-columns st-full-bg-columns-1">
        <div class="container">
            <div class="row st-tbl-row">
                <div class="col-sm-6" id="mision">
                    <div class="st-full-bg-col-in">
                        <h2 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">misión</h2>
                        <p class=" st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">Nuestro único propósito: ¡Inspirar el Planeta!<br><br>
En Stevia One, hacemos de la empresa un vector de desarrollo para las personas, el planeta y la economía global. Queremos inspirar a otros a construir un mundo mejor, generando valor para todos: clientes, trabajadores, accionistas, proveedores, la comunidad y el planeta.</p>
                    </div>
                </div>
                <div class="col-sm-6" id="vision">
                    <div class="st-full-bg-col-in">
                        <h2 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">visión</h2>
                        <p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">Somos una organización de personas motivadas por la felicidad, que llevará a Stevia One a convertirse en líder global, con un proceso de producción sostenible y responsable que impacte la vida de las personas y la salud del planeta.</p>
                    </div>
                </div>
            </div>          
        </div>
    </div>
    <div class="st-common-sec st-centered-image-sec st-valores-sec" id="valores">
        <div class="container">
            <h2 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">valores</h2>
            <ul class="st-bullet-list st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
                <li>Somos íntegros y transparentes. </li>
                <li>Felicidad. Somos apasionados en lo que hacemos.</li>
                <li>La sostenibilidad ante todo. </li>
                <li>Somos creativos, originales e innovadores. </li>
                <li>Comunicación colaborativa. Escuchamos y aprendemos de los otros. </li>
                <li>Excelencia Operacional. </li>
                <li>Entregamos resultados sin excusas. </li>
            </ul>
        </div>
    </div>

    <div class="st-common-sec st-info-sec st-info-sec-1">
        <div class="container masqueBx">
            <h3 class="st-green st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.2s" style="animation-delay: 0.2s;">Stevia One,<br>¡Queremos  Inspirar el Planeta!
</h3>
        </div>
    </div>

    

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                    NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->


    <div class="st-newsletter-hidden"><?php include('include/newsletter.php') ?></div>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</section>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<?php include('include/footer.php'); ?>