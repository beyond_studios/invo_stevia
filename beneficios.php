<?php include('include/header.php'); ?>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area st-header-1 st-header-beneficios" style="background-image:url('images/beneficios-1.jpg')">
    <div class="container">
        <div class="st-tbl">
            <div class="st-tbl-cell">
                <h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">beneficios</h1>
            </div>
        </div>
    </div>
</section>
<section class="st-middle-sec">

    <div class="st-common-sec st-info-sec st-big-desc st-beneficios-desc-sec">
        <div class="container">
            <h3 class="st-color-secondary st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s" >¡Gana salud y bienestar!</h3>
            <p class="st-font-light st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.4s"><strong>Sabías que…</strong></p>
<p class="st-font-light st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.4s">Los glicósidos de steviol –el extracto de la planta de stevia− son cero calorías, no afectan la presión arterial ni los niveles de glucosa en la sangre ni tampoco ponen en riesgo los dientes.</p>            
            <p class="st-font-light st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.4s"><strong>Te imaginas</strong></p>            
            <p class="st-font-light st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.4s">¿Una vida con productos a base de stevia?<br>
            En <a href="javascript:void(0)" class="st-link-secondary">Stevia One</a>, ¡Sí!</p>
        </div>
    </div>

    <div class="st-common-sec st-full-bg-columns st-beneficios-img-sec">
        <div class="container-fluid">
            <div class="row st-tbl-row st-tbl-md-row">
                <div class="col-md-6" id="mision" style="background-image:url('images/la-diabetes-dejo.jpg')">
                    
                </div>
                <div class="col-md-6 st-bg-theme" id="vision">
                    <div class="st-full-bg-col-in">
                        <h3 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">La diabetes dejó de ser amarga</h3>
                        <p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">De acuerdo a los criterios de The Joint FAO/WHO Expert Committee on Food Additives (JECFA), los estudios clínicos demuestran que los <strong>glicósidos de steviol no afectan la presión arterial ni los niveles de glucosa en la sangre.</strong></p>
                        <p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s"><strong>Te imaginas</strong></p>
                        <p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">¿A los diabéticos y a los niños disfrutar los beneficios de la stevia?</p>
                        <p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s"><strong>En Stevia One, ¡Sí!</strong><br>
                                                ¡Diabéticos disfruten de la dulzura de la stevia!</p>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <div class="st-common-sec st-big-desc st-big-desc-1">
        <div class="container">
            <p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.4s"><strong>Sonríe sin caries</strong></p>
            <p class="st-font-light st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.4s">Sabías que… <br>La bacteria que causa las caries puede consumir la stevia, pero no puede digerirla y en consecuencia perece. Entonces, en la batalla de la stevia vs. las caries, la stevia gana.  </p>
        </div>
    </div>

    

    

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                    NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->


    <div class="st-newsletter-hidden"><?php include('include/newsletter.php') ?></div>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</section>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<?php include('include/footer.php'); ?>