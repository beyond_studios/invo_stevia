<?php include('include/header.php'); ?>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area st-header-1" style="background-image:url('images/stevia-web-acerca-stevia-bg-1.jpg')">
    <div class="container">
        <div class="st-tbl">
            <div class="st-tbl-cell">
                <h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Acerca de Stevia</h1>
            </div>
        </div>
    </div>
</section>
<section class="st-middle-sec">

    <div class="st-common-sec st-twocol-desc-sec st-acerca-web-sec">
        <div class="container">
            <div class="row">
                <div class="col-sm-6" id="mision">                    
                        <h2 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">acerca de stevia</h2>
                        <p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
                            <strong>La stevia es una planta nativa de Sudamérica.</strong> Por siglos, los pueblos de Brasil y Paraguay utilizan sus hojas para endulzar comidas y bebidas. <br><br>
<strong>El polvo de hojas de stevia endulza 30 veces más</strong> que el azúcar. <strong>El extracto de hojas de stevia </strong>puede ser <strong>400 veces más dulce</strong> que el azúcar. Y lo mejor es que <strong>¡Es cero calorías!</strong></p>                        
                    
                </div>
                <div class="col-sm-6" id="vision">                    
                        <h2 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Sabías que… </h2>                        
                        <ul class="st-bullet-list st-bullet-list-plain st-bullet-space st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
                            <li>La stevia<strong> es fotoestable y termoestable</strong> a 95ºC.</li>
                            <li>La stevia<strong> se conserva en óptimas condiciones hasta por 3 años</strong> aproximadamente.</li>                            
                        </ul>
                </div>
            </div>          
        </div>
    </div>

    <div class="st-common-sec st-info-sec st-info-sec-1 st-info-acerca-web-sec">
        <div class="container">
            <h3 class="st-animate st-color-secondary" data-os-animation="fadeInUp" data-os-animation-delay="0.2s" >¡Qué buena es la stevia!<br><span>Y el ingrediente secreto está en su potencial…</span></h3>
        </div>
    </div>

    

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                    NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->


    <div class="st-newsletter-hidden"><?php include('include/newsletter.php') ?></div>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</section>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<?php include('include/footer.php'); ?>