<?php include('include/header.php'); ?>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area st-header-1" style="background-image:url('images/somos-stevia-one-bg-2.jpg')">
    <div class="container">
        <div class="st-tbl">
            <div class="st-tbl-cell">
                <h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Campos</h1>
            </div>
        </div>
    </div>
</section>
<section class="st-middle-sec">

    <div class="st-common-sec st-full-bg-columns st-campos-sec">
        <div class="container container-lg">
            <div class="row st-tbl-row st-tbl-md-row st-tbl-align-middle">
                <div class="col-md-5" id="mision">
                    <h2 class="st-bot-margin-30">campos</h2>
                    <ul class="st-bullet-list st-bullet-space st-bullet-list-plain">
                        <li>¡No a la deforestación! Nuestra stevia solo se cultiva en tierras recuperadas de la degradación, conservando los bosques.</li>
                        <li>Nuestros campos están ubicados en Perú, en zonas privilegiadas para el crecimiento de la Stevia.</li>
                        <li>Con condiciones de clima ideales para la producción.</li>
                        <li>Nuestra maquinaria es diseñada por nosotros a la medida de nuestras necesidades.</li>
                        <li>Cultivamos y cosechamos todo el año.</li>
                        <li>Podemos asegurar el abastecimiento continuo.</li>
                    </ul>
                </div>
                <div class="col-md-7" id="vision">
                    <div class="st-full-bg-col-in composBx">
                        <img src="images/campos-map-1.png" class="img-responsive">
                    </div>
                </div>
            </div>        
        </div>
    </div>
    

    <div class="st-common-sec st-full-bg-columns st-campos-middle-sec st-campos-middle-sec-1">
        <div class="container container-lg">
            <div class="row st-tbl-row st-tbl-md-row st-tbl-align-middle">
                <div class="col-md-6">
                    
                        <h3>El mejor rendimiento de nuestros campos se debe a:</h3>
                        <ul class="st-bullet-list st-bullet-list-plain st-bullet-space">
                            <li>Sistema de riego tecnificado por goteo.</li>
                            <li>Fertirriego que permite manejar con precisión los nutrientes que cada planta  necesita. Este sistema contribuye a la conservación del agua y
    los suelos al evitar que los fertilizantes se percolen a capas inferiores y puedan contaminar las aguas subterráneas.</li>
                            <li>Proceso de protección de cultivos.</li>
                        </ul>
                    
                </div>
                <div class="col-md-6">
                    <div class="composBx">
                        <h3 class="st-color-secondary">En Stevia One, cosechamos cada día porque nuestros cultivos son escalonados. Y por si fuera poco, estamos investigando otras localidades del Perú para poder extender nuestros campos.</h3>
                    </div>
                </div>
            </div>          
        </div>
    </div>
    
    <div class="st-common-sec st-info-sec st-testimonials-sec">
        <div class="container">            
                
                <blockquote>
                    <p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;">“Trabajar en Stevia One es un reto y una satisfacción. <br>
Aquí, trabajamos duro e innovamos continuamente para desarrollar productos saludables.  <br><br>
Queremos alcanzar nuestras metas respetando las normas.<br>
Esta es una empresa responsable social y ambientalmente. Y eso me llena de orgullo”.</p>
                    <div class="st-author-info st-author-info-1 st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.2s" style="animation-delay: 0.2s;">
                        <h5>Edgar Aliaga</h5>
                        <span class="st-author-post">Jefe de Operaciones Fundo Naranjos</span>
                        <span class="st-author-company">Ingeniero Agrónomo</span>
                    </div>
                </blockquote>
                
            
        </div>
    </div>

    

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                    NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->


    <div class="st-newsletter-hidden"><?php include('include/newsletter.php') ?></div>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</section>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<?php include('include/footer.php'); ?>