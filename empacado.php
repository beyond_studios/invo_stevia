<?php include('include/header.php'); ?>



<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

												MIDDLE SECTION

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<section class="st-header-area" style="background-image:url('images/empacado-bg.jpg')">

	<div class="container">

		<div class="st-tbl">

			<div class="st-tbl-cell">

				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">empacado<small></small></h1>

			</div>

		</div>

	</div>

</section>

<section class="st-middle-sec">

	<div class="st-common-sec st-theme-sec">

		<div class="container">

			<div class="row">

                <div class="col-sm-1"></div>

                <div class="col-sm-9">

                	<h2 class="st-underline st-underline-primary st-color-primary st-bot-margin-30">empacado</h2>

	                <ul class="st-bullet-list st-bullet-space">

		                <li>Las hojas de stevia se prensan y se empacan herméticamente.</li>

		                <li>Las pacas se etiquetan para facilitar su identificación como parte del proceso de trazabilidad.</li>

		            </ul>

                </div>

                <div class="col-sm-2"></div>

			</div>

		</div>

	</div>

	





<?php include('include/newsletter.php') ?>

</section>





<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

												MIDDLE SECTION END

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->



<?php include('include/footer.php'); ?>