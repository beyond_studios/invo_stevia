<?php
include('include/header.php');
?>
<section class="st-header-area st-header-1" style="background-image: url('images/somos-bg.jpg')">
	<div class="container">
		<div class="st-tbl">
			<div class="st-tbl-cell">
				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Quiénes somos</h1>
			</div>
		</div>
	</div>
</section>
<section class="st-middle-sec">
	<div class="st-common-sec st-common-center-text">
		<div class="container">
			<p>En Stevia One nos dedicamos desde el 2009 a la investigación de la stevia. Y queremos, además, contribuir al crecimiento sostenible del planeta y cuidado del medio ambiente, sembrando, cultivando, cosechando y produciendo una stevia con un proceso único de extracción y purificación.</p>
			<p class="text-green">¡Sin químicos! ¡Sin alcohol!</p>
		</div>
	</div>
	<div class="st-common-sec st-full-bg-columns st-full-bg-columns-1">
		<div class="container">
			<div class="row st-tbl-row">
				<div class="col-sm-6">
					<div class="st-full-bg-col-in" id="mision">
						<h2 class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;">MISIÓN</h2>
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.3s" style="animation-delay: 0.3s;"><strong>Somos Apasionados por la Salud:</strong> globalizando la disponibilidad de nuestro extracto natural de stevia para su consumo, ayudamos a vivir en un mundo más saludable, combatiendo la diabetes y obesidad.</p>
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.3s" style="animation-delay: 0.3s;"><strong>Somos respetuosos con nuestros Stakeholders:</strong> todos forman parte de nuestro éxito.</p>
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.3s" style="animation-delay: 0.3s;"><strong>Somos Concientes del Medio Ambiente:</strong> pensamos y actuamos de forma sostenible a lo largo de todos nuestros procesos de producción company.</p>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="st-full-bg-col-in" id="vision">
						<h2 class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;">VISIÓN</h2>
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.3s" style="animation-delay: 0.3s;">Ser el proveedor mundial más importante de stevia producida de manera natural.</p>
					</div>
					<div class="st-full-bg-col-in" id="valores">
						<h2 class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;">VALORES</h2>
						<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.3s" style="animation-delay: 0.3s;">Somos <strong>Responsables</strong><br/>
						Somos <strong>Eficientes</strong><br/>
						Somos <strong>Respetuosos</strong><br/>
						Somos <strong>Consecuentes</strong></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
include('include/footer.php');