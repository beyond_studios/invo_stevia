<?php include('include/header.php'); ?>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                        MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<section class="st-middle-sec">
  
    <div class="st-scroll-section">
      <div id="st-discover-video" class="st-discover-video">      
    <video id="bg-video" class="video-js vjs-default-skin" muted
    height="100%"
    width="100%"
    poster="images/banner.jpg"
    loop controls >
    <p>
      Your browser doesn't support video. Please <a href="http://browsehappy.com/">upgrade your browser</a> to see the example.
    </p>
    <source src="videos/13666379.mp4" type="video/mp4">
      <source src="videos/13666379.webm" type="video/webm">
      </video>
    </div>
      <div class="container">
        <div class="st-middle-content">
          <div class="st-middle-content-in">
            <div class="st-video-leaf-1"><div class="st-video-in-leaf st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.9s"><img src="images/home-video-leaf.png" alt="Leaf" /></div></div>
            <div class="st-video-leaf-2"><div class="st-video-in-leaf  st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.9s"><img src="images/home-video-leaf-1.png" alt="Leaf" /></div></div>
            <div class="st-video-leaf-3"><div class="st-video-in-leaf st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.9s"><img src="images/home-video-leaf-2.png" alt="Leaf" /></div></div>
            <div class="st-video-leaf-4"><div class="st-video-in-leaf st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.9s"><img src="images/home-video-leaf-3.png" alt="Leaf" /></div></div>
            <h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">HEALTHY<br>SWEETNESS</h1>
            <ul class="st-info-row">
              <li class="st-animate st-anchor-sinquimicos" data-os-animation="fadeInUp" data-os-animation-delay="0.4s">
                <a href="stevia-tomorrow-today.php">
                  <span class="media">
                    <span class="media-left"><img src="images/without-alcohol.svg" class="img-responsive"></span>
                    <span class="media-body">                  
                      <strong>¡Sin químicos!  <br>¡Sin alcohol!</strong>
                      <span class="st-info-row-btn"><i class="sicon-plus"></i>Conoce más</span>
                    </span>
                  </span>
                </a>
              </li>
              <li class="st-animate st-anchor-trazable" data-os-animation="fadeInUp" data-os-animation-delay="0.6s">
                <a href="stevia-tomorrow-today-trazabilidad.php">
                  <span class="media">
                    <span class="media-left"><img src="images/totally-traceable.svg" class="img-responsive"></span>
                    <span class="media-body">                  
                      <strong>100% Trazables</strong>
                      <span class="st-info-row-btn"><i class="sicon-plus"></i>Conoce más</span>
                    </span>
                  </span>

                </a>
              </li>
              <li class="st-animate st-reinforest-anchor" data-os-animation="fadeInUp" data-os-animation-delay="0.8s">
                <a href="stevia-tomorrow-today-rainforest.php">
                  <span class="media ">
                    <span class="media-left"><img src="images/frog.svg" class="img-responsive"></span>
                    <span class="media-body">                  
                      <strong>Rainforest Alliance Certified™</strong>
                      <span class="st-info-row-btn"><i class="sicon-plus"></i>Conoce más</span>
                    </span>
                  </span>

                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
</section>
<div class="st-page-preloader" style="display:block;"></div>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                        MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<?php include('include/footer.php'); ?>