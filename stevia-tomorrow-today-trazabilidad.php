<?php include('include/header.php'); ?>





<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

					MIDDLE SECTION

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<section class="st-header-area st-header-2" style="background-image:url('images/trazabilidad-head.jpg')">

	<div class="container">

		<div class="st-tbl">

			<div class="st-tbl-cell">

				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0">100%<br>

				Trazables <small>Estamos integrados verticalmente.</small></h1>



			</div>

		</div>

		<a href="#st-middle-content" class="st-header-link st-smooth-scroll st-animate" data-os-animation="fadeInDown" data-os-animation-delay="0"><i class="sicon-arrow-l-down"></i></a>

	</div>

</section>

<section class="st-middle-sec" id="st-middle-content">

	

	<div class="st-common-sec st-round-icon-sec st-trazables-sec">

		<div class="container">

			<div class="row">

			  <div class="st-tbl-row st-rounded-icon-row">

				<div class="col-sm-4">

					<div class="st-main-icon-l text-right st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0">

						<i class="sicon-estamos"></i>

					</div>

				</div>

				<div class="col-sm-8">

					<h2 class="st-small-desc st-trazables-heading st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">Estamos integrados verticalmente. 

Identificamos, documentamos y 

registramos cada lote desde la semilla 

hasta el producto final. 

</h2>



				</div>

			</div>

			</div>

		</div>

	</div>

	

	<div class="st-common-sec st-info-sec st-info-icon-sec">

		<div class="container">

			<div class="row">

				<div class="col-sm-4">

					<i class="sicon-trazables"></i>
					<p class="st-info-block st-animate"  data-os-animation="fadeInUp" data-os-animation-delay="0s">

						Únicos trazables al 100%<br>durante todo el proceso

					</p>

				</div>

				<div class="col-sm-4">

					<i class="sicon-shipping"></i>
					<p class="st-info-block st-animate"  data-os-animation="fadeInUp" data-os-animation-delay="0.4s">

						Desde la biofábrica<br>

						hasta el producto final

					</p>

				</div>

				<div class="col-sm-4">

					<i class="sicon-field"></i>
					<p class="st-info-block st-animate"  data-os-animation="fadeInUp" data-os-animation-delay="0.8s">

						Campos propios para<br>

						nuestros cultivos

					</p>

				</div>

			</div>

		</div>

	</div>

	<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                    NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
    <div class="st-newsletter-hidden"><?php include('include/newsletter.php');?></div>
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</section>





<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

												MIDDLE SECTION END

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->



<?php include('include/footer.php'); ?>