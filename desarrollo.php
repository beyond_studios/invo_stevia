<?php include('include/header.php'); ?>



<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

												MIDDLE SECTION

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<section class="st-header-area st-header-1" style="background-image:url('images/contact-bg-2-1.jpg')">

	<div class="container">

		<div class="st-tbl">

			<div class="st-tbl-cell">

				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Investigación y<br>Desarrollo (I&amp;D)</h1>

			</div>

		</div>

	</div>

</section>

<section class="st-middle-sec">

	<div class="st-common-sec st-twocol-desc-sec">

		<div class="container">

			<div class="st-biofabricaBx">

				<h2 >Investigación y<br>Desarrollo (I&D)</h2>

				<div class="row">

					<div class="col-md-6">

						<ul class="st-bullet-list st-bullet-list-plain st-bullet-space">

							<li>Es transversal a todas las etapas del proceso.</li>

							

							<li>El rendimiento y calidad genética de las plantas de Stevia One se encuentran entre los estándares más altos del mundo.</li>

						</ul>

					</div>

					<div class="col-md-6">						
						<ul class="st-bullet-list st-bullet-list-plain st-bullet-space">
							<li>El material genético es el resultado de una investigación y mejora genética continua.</li>
							<li>Contamos con equipos propios de HPLC para controlar la calidad de cada lote de producción.</li>
	
							

						</ul>

					</div>

				</div>

			</div>

		</div>

	</div>

	

    <div class="st-common-sec st-info-sec st-testimonials-sec">

        <div class="container masqueBx">

            
                <blockquote>

                    <p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s" >“Stevia One es mi trabajo, y es también el proyecto donde siento que día a día soy parte de un cambio macro.  Sí, eso fue lo que me gustó desde el inicio. Y es lo que se siente desde el trabajador más sencillo hasta los directores.</p><p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">Stevia One es un ejemplo de que las empresas pueden hacer las cosas bien, de que pueden ganar y al mismo tiempo crear bienestar para la gente y el planeta”.</p>

                    <div class="st-author-info st-author-info-1 st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">

                        <h5>Alejandro “Jano” Segovia</h5>

                        <span class="st-author-post">Gerente de R&amp;D</span>

                        <span class="st-author-company">Ingeniero Agrónomo</span>

                    </div>

                </blockquote>

                

        </div>

    </div>

	





<div class="st-newsletter-hidden"><?php include('include/newsletter.php') ?></div>

</section>





<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

												MIDDLE SECTION END

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->



<?php include('include/footer.php'); ?>