<?php include('include/header.php'); ?>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area st-header-1" style="background-image:url('images/biofabrica-bg-1.jpg')">
	<div class="container">
		<div class="st-tbl">
			<div class="st-tbl-cell">
				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Biofábrica</h1>
			</div>
		</div>
	</div>
</section>
<section class="st-middle-sec">
	<div class="st-common-sec st-biofabrica-sec">
		<div class="container container-lg">
			<div class="st-biofabricaBx">
				<h2>Biofábrica</h2>
				<div class="row">
					<div class="col-md-6">
						<h3 class="st-small-heading st-bot-margin-30">El trabajo realizado en nuestros laboratorios es muy importante para el mejoramiento genético vegetal.</h3>
					</div>
					
					<div class="col-md-6">
						<ul class="st-bullet-list st-bullet-space">
							<li>Permite la multiplicación exponencial de individuos de alto vigor genético y libres de plagas y enfermedades.</li>
							<li>La biofábrica cuenta con un banco de germoplasma que nos permite conservar un back-up de nuestras variedades, así como variedades externas para investigación.</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="st-common-sec st-info-sec st-testimonials-sec st-biofabrica-testimonial-sec">
		<div class="container masqueBx">			
				<blockquote>
					<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;">“Trabajo en Stevia One desde hace más de un año, y doy gracias por cada oportunidad que me han dado, me ayudan a descubrir nuevos potenciales en mí.<br><br>
Yo estoy segura que la stevia va a ayudar al mundo a tener una mejor calidad de vida en el futuro”.</p>
					<div class="st-author-info st-author-info-1 st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.2s" style="animation-delay: 0.2s;">
						<h5>Luz Marina Díaz Rodríguez</h5>
						<span class="st-author-post">Técnica de Biofábrica</span>
					</div>
				</blockquote>
		</div>
	</div>
	




<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->


	<div class="st-newsletter-hidden"><?php include('include/newsletter.php') ?></div>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</section>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<?php include('include/footer.php'); ?>