<?php include('include/header.php'); ?>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area st-header-1" style="background-image:url('images/secado-1.jpg')">
    <div class="container">
        <div class="st-tbl">
            <div class="st-tbl-cell">
                <h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Planta de secado</h1>
            </div>
        </div>
    </div>
</section>
<section class="st-middle-sec">

    <div class="st-common-sec st-planta-secado-sec">
        <div class="container">
            <div class="row st-tbl-row st-tbl-md-row st-tbl-align-middle">
                <div class="col-md-6" id="mision">
                    <div class="composBx">
                        <h2>planta de secado</h2>
                        <h6 class="st-grey">Luego de su cosecha, las hojas de stevia se someten a un proceso de secado.</h6>
                        <ul class="st-bullet-list st-bullet-list-plain st-bullet-space">
                            <li>Este es un proceso de secado indirecto y limpio.</li>
                            <li>Las hojas de stevia nunca entran en contacto con ningún producto químico porque no los hay.</li>
                            <li>Cada lote de producción hoja seca tiene trazabilidad total y es sometido a un estricto control de calidad.</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6" id="vision">
                    <div class="secodoBx">
                        <h5 class="st-color-secondary">En Stevia One, realizamos controles periódicos de la calidad de la hoja seca para asegurar que el insumo principal de la planta de producción cumpla con los estándares que se requieren para la producción del producto final.</h5>
                    </div>
                </div>
            </div>  
            <div class="row st-tbl-row st-tbl-md-row">
                <div class="col-md-6" id="mision">
                    <div class="composBx">
                        <h2>empacado</h2>                        
                        <ul class="st-bullet-list st-bullet-list-plain st-bullet-space">
                        <li>Las hojas de stevia se prensan y se empacan herméticamente.</li>
                        <li>Las pacas se etiquetan para facilitar su identificación como parte del proceso de trazabilidad.</li>
                    </ul>
                    </div>
                </div>
                <div class="col-md-6" id="vision"></div>
            </div>          
        </div>
    </div>


    

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                    NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->


    <div class="st-newsletter-hidden"><?php include('include/newsletter.php') ?></div>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</section>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<?php include('include/footer.php'); ?>