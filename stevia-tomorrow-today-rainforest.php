<?php include('include/header.php'); ?>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
					MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area st-header-2" style="background-image:url('images/rainforest_1-1.jpg')">
	<div class="container">
		<div class="st-tbl">
			<div class="st-tbl-cell">
				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0">Rainforest<br>
				Alliance Certified™<small>Somos los primeros y únicos</small></h1>

			</div>
		</div>
		<a href="#st-middle-content" class="st-header-link st-smooth-scroll st-animate" data-os-animation="fadeInDown" data-os-animation-delay="0"><i class="sicon-arrow-l-down"></i></a>
	</div>
</section>
<section class="st-middle-sec" id="st-middle-content">
	
	<div class="st-common-sec st-round-icon-sec st-rainforest-sec">
		<div class="container">
			<div class="row">
			  <div class="st-tbl-row st-rounded-icon-row">
				<div class="col-sm-3">
					<div class="st-rounded-icon-box st-rounded-icon-box-lime-green st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0">
						<div class="st-rounded-icon-box-in">
							<span class="st-rounded-icon-wrap">
								<img src="images/rainforest-1.svg" alt="Rainforest Alliance Certified">
							</span>							
						</div>
					</div>
				</div>
				<div class="col-sm-9">
					<h2 class="st-small-desc st-rainforest-heading st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">Somos los primeros y únicos productores 
de stevia con el sello <strong>Rainforest 
Alliance Certified™</strong></h2>
					<p class="st-round-icon-desc st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.5s">Nuestra stevia proviene de campos donde el agua, el 
suelo y el ecosistema son conservados, los trabajadores 
son tratados de manera justa y las comunidades 
locales se benefician.

</p>

				</div>
			</div>
			</div>
		</div>
	</div>
	
	<div class="st-common-sec st-info-sec st-rainforest-info-sec">

		<div class="container container-lg">

			<div class="st-biofabricaBx">
				<h2>Rainforest Alliance Certified™</h2>
				<div class="trazablesBx">					
					<p>Después de mucho esfuerzo y trabajo continuo, en Stevia One nos enorgullece contar con el sello Rainforest Alliance Certified™. Somos capaces de transformar las prácticas de uso de suelo, las prácticas empresariales y el comportamiento de los consumidores, entre otros.</p>
					<p>El sello Rainforest Alliance Certified™ es un símbolo reconocido internacionalmente de sostenibilidad ambiental, social y económica. Estos ejes se rigen a su vez bajo la norma para Agricultura Sostenible dictada por la Red de Agricultura Sostenible (RAS), otra organización que trabaja en conjunto con Rainforest Alliance.</p>
				</div>

			</div>

		</div>

	</div>

	

    <div class="st-common-sec st-round-icon-sec st-rainforest-listings">

		<div class="container rainBx">

			<h5>La norma está basada en <strong>10 principios:</strong></h5>

			

					<ol class="st-ol-listing st-ol-listing-plain st-bullet-space st-listing-twocol" start="0">

						<li><h4>Sistema de gestión social y ambiental</h4>

							<p>Son las políticas y procedimientos que se utilizan para planificar y ejecutar las operaciones que fomenten la implementación de las buenas prácticas de manejo.</p>

						</li>

						<li><h4>Salud y seguridad ocupacional</h4>

							<p>Los campos certificados cuentan con un programa de salud y seguridad ocupacional para reducir o prevenir los riesgos de accidentes en el lugar de trabajo.</p>

						</li>

						<li><h4>Conservación de ecosistemas</h4>

							<p>Los campos certificados protegen los ecosistemas naturales y realizan actividades para recuperar ecosistemas degradados.</p>

						</li>

						<li><h4>Relaciones con la comunidad</h4>

							<p>Los campos certificados se relacionan positivamente con las comunidades cercanas y los grupos de interés locales. Contribuyen al desarrollo económico local mediante la capacitación y el empleo.</p>

						</li>

						<li><h4>Protección de la vida silvestre</h4>

							<p>Los campos certificados son refugios para la vida silvestre residente y migratoria, especialmente para las especies amenazadas o en peligro de extinción.</p>

						</li>

						<li><h4>Manejo integrado del cultivo.</h4>

							<p>Los campos certificados contribuyen a la eliminación del uso de productos químicos reconocidos por su impacto negativo en la salud humana y los recursos naturales. El manejo integrado de cultivos disminuye los riesgos y efectos de infestaciones de plagas.</p>

						</li>

						<li><h4>Conservación de recursos hídricos</h4>

							<p>El agua es vital. Los campos certificados realizan acciones para conservar el agua, evitar su desperdicio y prevenir su contaminación.</p>

						</li>

						<li><h4>Manejo y conservación de suelos.</h4>

							<p>Los campos certificados realizan actividades para prevenir o controlar la erosión y así disminuir la pérdida de nutrientes y los impactos negativos en los cuerpos de agua.</p>

						</li>

						<li><h4>Trato justo y buenas condiciones para los trabajadores</h4>

							<p>Los trabajadores que laboran en campos certificados y las familias que viven en ellos gozan de derechos y condiciones expresados por la ONU en la Declaración Universal de los Derechos Humanos, en la Convención sobre los Derechos de Niños y los convenios de la OIT.</p>

						</li>

						<li><h4>Manejo integrado de desechos.</h4>
							<p>Los campos certificados están ordenados y limpios, sus trabajadores y habitantes cooperan con el aseo y manejan los desechos mediante actividades de reciclaje, reducción y reutilización.</p>

						</li>

					</ol>
		</div>

	</div>

	<div class="st-common-sec st-medal-sec">
		<div class="container">
			<div class="st-medal-icon-bg"><i class="sicon-medal"></i></div>
			<div class="st-medal-icon"><i class="sicon-medal"></i></div>
			<h3 class="st-color-secondary st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.2s" style="animation-delay: 0.2s;">Stevia One <span class="disblk">Somos los primeros productores de stevia con el sello Rainforest Alliance Certified™</span></h3>
		</div>
	</div>

	<div class="st-common-sec st-info-sec st-testimonials-sec">
		<div class="container container-sm">
			
				<blockquote>
					<p class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">“Stevia One le ha cambiado la vida a la gente de la zona. Más allá de un trabajo o un sueldo fijo mensual. La gente del campo se siente orgullosa de tener un trabajo justo en una empresa responsable”.</p>
					<div class="st-author-info st-author-info-1 st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">
					<h5>Julia Aredo </h5>
					<span class="st-author-post">Jefa de Campo Fundo Calzada</span>
					<span class="st-author-company">Agrónoma</span>
								</div>
				</blockquote>
			
		</div>
	</div>
	

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->


	<div class="st-newsletter-hidden"><?php include('include/newsletter.php'); ?></div>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</section>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<?php include('include/footer.php'); ?>