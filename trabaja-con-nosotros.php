<?php include('include/header.php'); ?>
<section class="st-header-area st-header-1" style="background-image: url('images/background-unique-proposal.png')">
	<div class="container">
		<div class="st-tbl">
			<div class="st-tbl-cell">
				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Trabaja con nosotros</h1>
			</div>
		</div>
	</div>
</section>
<section class="st-middle-sec">
	<div class="st-common-sec st-contact-trabaja-desc">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h2>Somos Stevia One, una empresa agroindustrial<br/>Peruana, fundada en el año 2009.</h2>
					<p>Contamos con operaciones en la Selva Centro y la Costa norte del País. Estamos dedicados  al sembrado, cultivo, cosecha y extracción de Stevia, de manera natural.</p>
					<p>Nuestro proceso único, es respetuoso con el Medio Ambiente, y maneja altos estándares de Calidad y Eficiencia.</p>
					<p>Nuestro producto, asegura su trazabilidad y el buen sabor.</p>
					<p>Nuestra gente, es consecuente con nuestra filosofía.  Personas "apasionadas por la Salud".</p>
					<p>Con Stevia One tendrás la oportunidad de participar en una empresa, que busca contribuir para que nuestro mundo sea más saludable.</p>
					<p>Estamos orientados a lograr el crecimiento, de todas las partes interesadas. Nuestros colaboradores, proveedores, clientes y accionistas.</p>
					<p class="boxed">Si estás listo para ser parte de nuestro equipo, envíanos tu CV junto con una carta de motivación a<br/><a href="mailto:seleccion@steviaone.com">seleccion@steviaone.com</a></p>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
include('include/footer.php');
