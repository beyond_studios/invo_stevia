require "./remove-all-comments-monkey-patch"
require 'fileutils'

relative_assets = true
css_dir = "css"
sass_dir = "scss"
images_dir = "images"
fonts_dir = "fonts"
output_style = :compressed
line_comments = false
preferred_syntax = :scss
sass_options = {:sourcemap => true}
on_stylesheet_saved do |file|
  if File.exists?(file)
    filename = File.basename(file, File.extname(file))
    File.rename(file, "css" + "/" + filename + ".min" + File.extname(file))
  end
end
