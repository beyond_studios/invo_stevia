<?php include('include/header.php'); ?>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area st-header-1" style="background-image:url('images/certificaciones-1.jpg')">
    <div class="container">
        <div class="st-tbl">
            <div class="st-tbl-cell">
                <h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Certificaciones</h1>
            </div>
        </div>
    </div>
</section>
<section class="st-middle-sec">
    <div class="st-common-sec st-round-icon-sec st-certificaciones-sec">
        <div class="container">
            
                    <div class="st-rounded-icon-box st-rounded-icon-box-lime-green st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0">
                        <div class="st-rounded-icon-box-in">
                            <span class="st-rounded-icon-wrap">
                                <img src="images/rainforest.svg" alt="Rainforest Alliance Certified">
                            </span>                            
                        </div>
                    </div>
                    <p class="st-round-icon-desc st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.5s">El sello Rainforest Alliance Certified™ es un símbolo reconocido internacionalmente de sostenibilidad ambiental, social y económica. Estos ejes se rigen a su vez bajo la norma para Agricultura Sostenible dictada por la Red de Agricultura Sostenible (RAS), otra organización que trabaja en conjunto con Rainforest Alliance.</p>
                    <a href="stevia-tomorrow-today-rainforest.php" class="btn btn-link btn-sm st-animate" data-os-animation="fadeInUp" data-os-animation-delay="1s">Leer más</a>
                    <h4 class="st-md-heading st-animate st-color-secondary" data-os-animation="fadeInUp" data-os-animation-delay="1.5s"><a href="http://www.rainforest-alliance.org" >www.rainforest-alliance.org</a></h4>

        </div>
    </div>


    <!-- <div class="st-common-sec st-full-bg-columns">
        <div class="container-fluid">
            <div class="row st-tbl-row">
                <div class="col-sm-6">
                    <div class="st-full-bg-col-in">                        
                        <div class="row st-profile-row">
                            <div class="col-sm-4 st-small-profile-col">
                                <span class="st-profile-pic-wrap st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0">
                                    <img src="images/profile-6.jpg" alt="Filip Balcaen" class="img-responsive" draggable="false">
                                </span>
                                <div class="st-author-info st-author-info-bottom st-author-info-no-underline text-center st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">
                                  <h5>Lorem Ipsum</h5>                                  
                                </div>
                            </div>
                            <div class="col-sm-8 st-small-profile-desc-col">
                                 <p class="st-animate st-font-medium st-underline st-underline-primary st-underline-thin" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ultrices eu turpis et condimentum. Quisque ac arcu mollis, auctor tortor in, interdum nisl.</p>
                                 <h4 class="st-md-heading st-animate st-color-secondary" data-os-animation="fadeInUp" data-os-animation-delay="1s"><a href="http://www.rainforest-alliance.org" >www.rainforest-alliance.org</a></h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 st-bg-gray">
                    <div class="st-full-bg-col-in">                        
                        <div class="row st-profile-row">
                            <div class="col-sm-4 st-small-profile-col">
                                <span class="st-profile-pic-wrap st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0">
                                    <img src="images/profile-2.jpg" alt="Sebastiaan Saverys" class="img-responsive" draggable="false">
                                </span>
                                <div class="st-author-info st-author-info-bottom st-author-info-no-underline text-center st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">
                                  <h5>Lorem Ipsum</h5>                                  
                                </div>
                            </div>
                            <div class="col-sm-8 st-small-profile-desc-col">
                                
                                    <p class="st-animate st-font-medium st-underline st-underline-primary st-underline-thin" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ultrices eu turpis et condimentum. Quisque ac arcu mollis, auctor tortor in, interdum nisl.</p>
                                    <h4 class="st-md-heading st-animate st-color-secondary" data-os-animation="fadeInUp" data-os-animation-delay="1s"><a href="http://www.rainforest-alliance.org" >www.rainforest-alliance.org</a></h4>
                                                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>


          
                     
        </div>
    </div>-->
    

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                    NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->


    <div class="st-newsletter-hidden"><?php include('include/newsletter.php') ?></div>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</section>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<?php include('include/footer.php'); ?>