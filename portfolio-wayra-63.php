<?php include('include/header.php'); ?>
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area" style="background-image: url('images/banner_wayra63.jpg')">
	<div class="container">
		<div class="st-tbl">
			<div class="st-tbl-cell">
				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">
					WAYRA-63<small></small>
				</h1>
			</div>
		</div>
	</div>
</section>
<section class="st-middle-sec">
	<div class="st-common-sec st-full-bg-columns">
		<div class="container-fluid">
			<div class="row st-tbl-row">
				<div class="col-sm-6 st-bg-theme" id="mision">
					<div class="st-full-bg-col-in">
						<h2 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">DESCRIPCIÓN</h2>
						<p class="st-underline st-underline-secondary st-animate st-underline-space-50 st-top-margin-25" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">SG95 Reb A 63 de esteviol glucósidos es un edulcorante natural no calórico y extraído de las hojas de la planta de stevia. No contiene calorías, posee un alto dulzor entre 150 - 200 veces más dulce que el azúcar.</p>
					</div>
				</div>
				<div class="col-sm-6" id="vision">
					<div class="st-full-bg-col-in">
						<h2 class="st-color-primary st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">CARACTERÍSTICAS</h2>
						<ul class="st-bullet-list st-animate st-underline st-underline-primary st-underline-space-50 animated fadeInUp st-top-margin-25" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
							<li>Lotrem ipsum dolor sit amet, consectetur adipiscing elit.</li>
							<li>Ut at ante sodales, molestie massa eu, sodales enim.</li>
							<li>Phasellus mattis lorem at gravida orci blandit.</li>
							<li>In tempus erat quis ligula rutrum aliquet.</li>
							<li>Nam in libero maximus, ultrices enim icorper nisl.</li>
							<li>Mauris et sapien a velit lobortis viverra.</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="st-common-sec st-image-sec" style="background-image: url('images/applicationes-bg.jpg')">
		<div class="container container-xl">
			<h2 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">aplicaciones</h2>
			<div class="row">
				<div class="col-sm-6">
					<ul class="st-bullet-list st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
						<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
						<li>Ut at ante sodales, molestie massa eu, sodales enim.</li>
						<li>Phasellus mattis lorem at gravida orci blandit.</li>
						<li>In tempus erat quis ligula rutrum aliquet.</li>
						<li>Nam in libero maximus, ultrices enim icorper nisl.</li>
					</ul>
				</div>
				<div class="col-sm-6">
					<ul class="st-icon-list st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.5s">
						<li><span>Solicitar Muestra</span><i class="sicon-muestra"></i></li>
						<li><span>Descargar Ficha Técnica</span><i class="sicon-d-arrow-down"></i></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="st-common-sec st-product-detail-big-btns-sec">
		<div class="container">
			<div class="row st-big-btn-row">
				<div class="col-xs-6 text-right">
					<a href="#" class="xtra-lg-btn st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s"><i class="sicon-arrow-s-left btnArrow"></i><span>ver nuestros<br>anterior
					</span></a>
				</div>
				<div class="col-xs-6">
					<a href="#" class="xtra-lg-btn st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.5s"><span>ver producto<br> siguiente
					</span><i class="sicon-arrow-s-right btnArrow"></i></a>
				</div>
			</div>
		</div>
	</div>
	<div class="container container-xl">
		<hr class="st-hr-space">
	</div>
	<div class="st-common-sec st-product-detail-form st-contact-form-sec">
		<div class="container">
			<h3 class="st-title-underline st-green st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">Contáctese con nosotros</h3>
			<?php include('include/contact.php')?>
		</div>
	</div>
	<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                    NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
    <?php include('include/newsletter.php')?>
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
</section>
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<?php include('include/footer.php'); ?>