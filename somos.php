<?php include('include/header.php'); ?>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area st-header-1" style="background-image:url('images/somos-bg.jpg')">
	<div class="container">
		<div class="st-tbl">
			<div class="st-tbl-cell">
				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">SOMOS STEVIA ONE</h1>
			</div>
		</div>
	</div>
</section>
<section class="st-middle-sec">
	<div class="st-common-sec st-somos-video-sec">
		<div class="container container-md st-md-content">
			<div class="st-vimeo-video-bx st-vimeo-video-bx-wide">
			   <div class="embed-responsive embed-responsive-16by9">
				<iframe src="https://player.vimeo.com/video/189227912" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>
			</div>
		</div>
	</div>
	<div class="st-common-sec st-theme-grey-sec">
		<div class="container container-md st-md-content">
			<div class="st-somosBx">
				<p>En Stevia One nos dedicamos desde el 2009 a la investigación de la Stevia. Y queremos, además, contribuir al crecimiento sostenible del planeta y cuidado del medio ambiente, sembrando, cultivando, cosechando y produciendo una Stevia con un proceso único de extracción y purificación.</p>
				<h3 class="st-grey st-animate animated fadeInUp">¡Sin químicos! ¡Sin alcohol!</h3>
			</div>
		</div>
	</div>
	<div class="st-common-sec st-info-sec st-testimonials-sec">
		<div class="container">
			<div class="row">
				<div class="col-md-1"></div>
				<blockquote class="col-md-10">
					<p class="st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="animation-delay: 0s;">“Me siento orgullosa de trabajar en Stevia One porque la gente es participativa y colaboradora. Todos nos apoyamos. Somos un equipo. Además, agradezco cada oportunidad porque aquí nos capacitan. Nuestro trabajo va a ser bueno para todos, y no solo en Perú, sino en el mundo”.</p>
					<div class="st-author-info st-author-info-1 st-animate animated fadeInUp" data-os-animation="fadeInUp" data-os-animation-delay="0.2s" style="animation-delay: 0.2s;">
						<h5>Erlinda Calle Rojast</h5>
						<span class="st-author-post">Erlinda Calle Rojast</span>
					</div>
				</blockquote>
				<div class="col-md-1"></div>				
			</div>
		</div>
	</div>
	




<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                    NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
    <div class="st-newsletter-hidden"><?php include('include/newsletter.php');?></div>
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</section>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<?php include('include/footer.php'); ?>