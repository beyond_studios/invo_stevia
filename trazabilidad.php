<?php include('include/header.php'); ?>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												MIDDLE SECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<section class="st-header-area st-header-1" style="background-image:url('images/trazabilidad-1.jpg')">
	<div class="container">
		<div class="st-tbl">
			<div class="st-tbl-cell">
				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Trazabilidad</h1>
			</div>
		</div>
	</div>
</section>
<section class="st-middle-sec">	
		<div class="st-common-sec st-twocol-desc-sec">
			<div class="container">
				<div class="st-biofabricaBx">
					<h2 class="st-color-secondary st-animate"  data-os-animation="fadeInUp" data-os-animation-delay="0.2s">100% trazables</h2>
					<div class="row trazablesBx">
						<div class="col-md-6 st-animate"  data-os-animation="fadeInUp" data-os-animation-delay="0.2s">
							<h5>Identificamos, documentamos y registramos cada etapa del proceso de producción, desde la semilla hasta el producto final de stevia.
							</h5>
							<h6 class="st-color-primary">Trazabilidad</h6>
							<p>En Stevia One, conocemos cada planta de cada lote, sabemos quienes trabajaron, cuántas horas y qué días.</p>
						</div>					
						<div class="col-md-6 st-animate"  data-os-animation="fadeInUp" data-os-animation-delay="0.4s">
							<p>Registramos con exactitud en qué fecha se cosechó el lote, de qué campo proviene, en qué días se envió a planta de producción, así como todos los insumos utilizados.</p>
							<p>Gracias a la trazabilidad, todo el histórico queda registrado. Y en caso de necesidad, tenemos la capacidad para identificar el origen y evolución de cada lote de inmediato.</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="st-common-sec st-info-sec st-info-sec-2 st-trazabilidad-info-sec">
			<div class="container masqueBx">
				<h3 class="st-green st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.6s"><span class="disblk">La trazabilidad es una de las ventajas competitivas de Stevia One.</span><br>Conocemos y controlamos cada etapa del proceso de producción de principio a fin.</h3>
			</div>
		</div>

	




<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												NEWSLETTER SECTION START
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->


	<div class="st-newsletter-hidden"><?php include('include/newsletter.php') ?></div>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												NEWSLETTER SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</section>


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
												MIDDLE SECTION END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<?php include('include/footer.php'); ?>