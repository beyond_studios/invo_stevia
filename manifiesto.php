<?php include('include/header.php'); ?>



<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

												MIDDLE SECTION

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<section class="st-header-area st-header-1" style="background-image:url('images/somos-bg.jpg')">

	<div class="container">

		<div class="st-tbl">

			<div class="st-tbl-cell">

				<h1 class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0s">Manifiesto</h1>

			</div>

		</div>

	</div>

</section>

<section class="st-middle-sec">

	<div class="st-common-sec">

		<div class="container container-md st-md-content st-manifesto-content">

			<p>¿Puede una hoja dulce de stevia cambiar la vida de millones de personas?</p>

			<p>¿Puede una pequeña planta como la stevia revolucionar la industria de alimentos?</p>

			<p><strong>Sí. En Stevia One, estamos seguros que sí.</strong></p>

			<p>¿Puede una empresa ser responsable, consciente y sostenible?</p>
			<p>¿Puede una empresa lograr un impacto positivo en el planeta?</p>
			<p><strong>En Stevia One, creemos que somos capaces de eso y más. Nosotros estamos revolucionando la forma de hacer empresa.</strong></p></div>

	</div>

	<div class="st-big-image-panel st-big-image-panel-1" style="background-image:url('images/manifiesto-inner-bg-1.jpg')">

		<div class="container">

			<p class="st-small-txt"><span>Este es</span> nuestro Manifiesto:</p>

			<p>En Stevia One, <br>

				Creemos en lo que hacemos.<br>

				Cuando haces lo que crees: Inspiras.<br>

				Y cuando inspiras: tu gente se compromete.</p>

			<p>Cuando cambias tú, cambia el mundo.<br>

				Stevia One, nuestro único propósito: <br>

				¡Inspirar el Planeta!</p>

		</div>

	</div>

	





<?php //include('include/newsletter.php') ?>

</section>





<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

												MIDDLE SECTION END

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->



<?php include('include/footer.php'); ?>