var messages =
{
	'en' :
	{
		required : "This field is required",
		email : "This is not a valid email"
	},
	'es' :
	{
		required : "Campo obligatorio",
		email : "El e-mail no es valido"
	}
};

(function()
{
	'use strict';
	var lang = $('html').attr('lang');
	$('.st-form-success, .st-form-error').hide();
	$('#formNewsletter').validate(
	{
		ignore : [],
		ignoreTitle : true,
		rules :
		{
			textBoxNombreCompleto :
			{
				required : true
			},
			textBoxEmail :
			{
				required : true,
				email : true
			}
		},
		messages :
		{
			textBoxNombreCompleto :
			{
				required : messages[lang].required
			},
			textBoxEmail :
			{
				required : messages[lang].required,
				email : messages[lang].email
			}
		},
		errorElement : 'span',
		errorPlacement : function(error, element)
		{
			$(error).addClass('helper-block');
			$(element).next().replaceWith($(error));
		},
		submitHandler : function(form)
		{
			var $button = $('button.btn-block', $(form));
			$button.attr('disabled', 'disabled');
			var data = $(form).serialize();
			$.ajax(
			{
				type : "POST",
				url : (($('html').attr('lang')=='es')?'send-newsletter.php':'../send-newsletter.php'),
				data : data,
				success : function(message)
				{
					var response = eval('(' + message + ')');
					var message = '#formNewsletter .st-form-success';
					if (response.isError)
						message = '#formNewsletter .st-form-error';
					$(message).slideDown();
					if (!response.isError)
					{
						$('#formNewsletter').slideUp();
						$('input[type="text"]', $(form)).val('');
					}
					setTimeout(function()
					{
						$(message).slideUp();
						$('#formNewsletter').slideDown();
					}, 3000);
					$button.removeAttr('disabled');
				},
				error : function()
				{
					$button.removeAttr('disabled');
				}
			});
		},
		focusInvalid : false
	});
})();
