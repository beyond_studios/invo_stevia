var messages =
{
	'en' :
	{
		required : "This field is required",
		email : "This is not a valid email"
	},
	'es' :
	{
		required : "Campo obligatorio",
		email : "El e-mail no es valido"
	}
};

(function()
{
	'use strict';
	var lang = $('html').attr('lang');
	$('.st-form-success, .st-form-error').hide();
	$('#formTrabaja').validate(
	{
		ignore : [],
		ignoreTitle : true,
		rules :
		{
			textBoxTelefono :
			{
				required : true
			},
			textBoxNombre :
			{
				required : true
			},
			textBoxApellidos :
			{
				required : true
			},
			fileUpload :
			{
				required : true
			},
			textBoxEmail :
			{
				required : true,
				email : true
			}
		},
		messages :
		{
			textBoxTelefono :
			{
				required : messages[lang].required
			},
			textBoxNombre :
			{
				required : messages[lang].required
			},
			textBoxApellidos :
			{
				required : messages[lang].required
			},
			fileUpload :
			{
				required : messages[lang].required
			},
			textBoxEmail :
			{
				required : messages[lang].required,
				email : messages[lang].email
			}
		},
		errorElement : 'span',
		errorPlacement : function(error, element)
		{
			$(error).addClass('helper-block');
			$(element).next().replaceWith($(error));
		},
		submitHandler : function(form)
		{
			var $button = $('button.btn-block', $(form));
			$button.attr('disabled', 'disabled');
			var data = $(form).serialize();
			$('#formTrabaja').ajaxSubmit(
			{
				type : "POST",
				url : (($('html').attr('lang') == 'es') ? 'send-workwithus.php' : '../send-workwithus.php'),
				success : function(message)
				{
					var response = eval('(' + message + ')');
					var message = '#formTrabaja .st-form-success';
					if (response.isError)
						message = '#formTrabaja .st-form-error';
					$(message).slideDown();
					if (!response.isError)
					{
						$('#panelContacto').slideUp();
						$('input[type="text"], input[type=email], input[type=file]', $(form)).val('');
						$('textarea', $(form)).val('');
						$('select[name=dropDownListOficina]', $(form)).selectpicker('val', (($('html').attr('lang') == 'es') ? 'Oficina Administrativa' : 'Comercial Office'));
						$('select[name=dropDownListPais]', $(form)).selectpicker('val', 'PE');
						$('.st-input-file-name').html('Adjuntar CV*')
					}
					setTimeout(function()
					{
						$(message).slideUp();
						$('#panelContacto').slideDown();
					}, 3000);
					$button.removeAttr('disabled');
				},
				error : function()
				{
					$button.removeAttr('disabled');
				}
			});
		},
		focusInvalid : false
	});
})();
