<?php
include('include/header.php');
?>
<section class="st-middle-sec">
	<div class="st-scroll-section">
		<div id="st-discover-video" class="st-discover-video">
			<video id="bg-video" class="video-js vjs-default-skin" muted height="100%" width="100%" poster="images/banner.jpg" autoplay loop controls>
				<p>
					Your browser doesn't support video. Please <a href="http://browsehappy.com/">upgrade your browser</a> to see the example.
				</p>
				<source src="videos/wt.mp4" type="video/mp4">
				<source src="videos/water.webm" type="video/webm">
			</video>
		</div>
		<div class="container">
			<div class="st-middle-content">
				<div class="st-middle-content-in" id='scene'>
					<div class="st-video-leaf-1">
						<div class="st-video-in-leaf st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.9s">
							<img class='layer'  data-depth="0.3" src="images/home-video-leaf.png" alt="Leaf" />
						</div>
					</div>
					<div class="st-video-leaf-2">
						<div class="st-video-in-leaf  st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.9s">
							<img class='layer'  data-depth="0.4" src="images/home-video-leaf-1.png" alt="Leaf" />
						</div>
					</div>
					<div class="st-video-leaf-3">
						<div class="st-video-in-leaf st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.9s">
							<img class='layer'  data-depth="0.5"  src="images/home-video-leaf-2.png" alt="Leaf" />
						</div>
					</div>
					<div class="st-video-leaf-4">
						<div class="st-video-in-leaf st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.9s">
							<img class='layer'  data-depth="0.2"  src="images/home-video-leaf-3.png" alt="Leaf" />
						</div>
					</div>

					<h1  class="st-animate" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
						HEALTHY<br>SWEETNESS
					</h1>
					<div class="scrolling-button">
						<div class='icon-scroll'></div>
					</div>
					<ul class="st-info-row"  data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
						<li class="st-animate st-anchor-sinquimicos animated  data-os-animation="fadeInUp" data-os-animation-delay="0.3s"">
							<a href="nuestra-propuesta-unica.php">
								<span class="media">
									<span class="media-left"><img src="images/without-alcohol.svg" class="img-responsive"></span>
									<span class="media-body">
										<strong>¡Sin químicos! <br>¡Sin alcohol!</strong>
										<span class="st-info-row-btn"><i class="sicon-plus"></i>Conoce más</span>
									</span>
								</span>
							</a>
						</li>
						<li class="st-animate st-anchor-trazable animated">
							<a href="nuestra-propuesta-unica.php">
								<span class="media">
									<span class="media-left"><img src="images/totally-traceable.svg" class="img-responsive"></span>
									<span class="media-body">
										<strong>100% Trazables</strong>
										<span class="st-info-row-btn"><i class="sicon-plus"></i>Conoce más</span>
									</span>
								</span>
							</a>
						</li>
						<li class="st-animate st-reinforest-anchor animated">
							<a href="nuestra-propuesta-unica.php" >
								<span class="media ">
									<span class="media-left"><img src="images/frog.svg" class="img-responsive"></span>
									<span class="media-body">
										<strong>Rainforest Alliance Certified™</strong>
										<span class="st-info-row-btn"><i class="sicon-plus"></i>Conoce más</span>
									</span>
								</span>
							</a>
						</li>
					</ul>
					
				</div>
			</div>
		</div>
	</div>
</section>

<?php
include('include/footer.php');