var gulp = require('gulp');
var postcss = require('gulp-postcss');
var sass = require('gulp-compass');
 
var autoprefixer = require('autoprefixer');
var cssnano = require('cssnano');
 
gulp.task('css', function () {
    var processors = [
        autoprefixer,
        cssnano
    ];
    return gulp.src('./scss/*.scss')
        .pipe(sass({
		config_file: 'config.rb',
		css: 'css',
		sass: 'scss'
	}).on('error', function(error)
	{
		console.log(error);
		this.emit('end');
	}))
        .pipe(postcss(processors))
        .pipe(gulp.dest('./css/'));
});
gulp.task('watch:css', function () {
    gulp.watch('./scss/*.scss', ['css']);
});
gulp.task('default', ['watch:css']);
